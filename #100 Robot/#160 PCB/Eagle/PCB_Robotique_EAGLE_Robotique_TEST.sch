<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="con-panduit">
<description>&lt;b&gt;Panduit Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="057-034-0">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
series 057 contact pc board low profile headers&lt;p&gt;
angled</description>
<wire x1="-1.9" y1="-0.23" x2="-1.9" y2="5.86" width="0.2032" layer="21"/>
<wire x1="-1.9" y1="-0.23" x2="1.9" y2="-0.23" width="0.2032" layer="21"/>
<wire x1="1.9" y1="-0.23" x2="1.9" y2="5.86" width="0.2032" layer="21"/>
<wire x1="-21.04" y1="5.22" x2="-20.34" y2="3.25" width="0.4064" layer="21"/>
<wire x1="-20.34" y1="3.25" x2="-19.64" y2="5.22" width="0.4064" layer="21"/>
<wire x1="-19.64" y1="5.22" x2="-21.04" y2="5.22" width="0.4064" layer="21"/>
<wire x1="-25.45" y1="-2.54" x2="-25.45" y2="5.86" width="0.2032" layer="21"/>
<wire x1="25.45" y1="5.86" x2="25.45" y2="-2.44" width="0.2032" layer="21"/>
<wire x1="-25.4" y1="-2.54" x2="-22.86" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-22.86" y1="-2.54" x2="-22.86" y2="-5.98" width="0.2032" layer="21"/>
<wire x1="25.4" y1="-2.54" x2="22.86" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="22.86" y1="-2.54" x2="22.86" y2="-6.05" width="0.2032" layer="21"/>
<wire x1="22.85" y1="-6.04" x2="-22.85" y2="-6.04" width="0.2032" layer="21"/>
<wire x1="-25.45" y1="5.86" x2="25.45" y2="5.86" width="0.2032" layer="21"/>
<pad name="1" x="-20.32" y="-5.08" drill="1" shape="octagon"/>
<pad name="2" x="-20.32" y="-2.54" drill="1" shape="octagon"/>
<pad name="3" x="-17.78" y="-5.08" drill="1" shape="octagon"/>
<pad name="4" x="-17.78" y="-2.54" drill="1" shape="octagon"/>
<pad name="5" x="-15.24" y="-5.08" drill="1" shape="octagon"/>
<pad name="6" x="-15.24" y="-2.54" drill="1" shape="octagon"/>
<pad name="7" x="-12.7" y="-5.08" drill="1" shape="octagon"/>
<pad name="8" x="-12.7" y="-2.54" drill="1" shape="octagon"/>
<pad name="9" x="-10.16" y="-5.08" drill="1" shape="octagon"/>
<pad name="10" x="-10.16" y="-2.54" drill="1" shape="octagon"/>
<pad name="11" x="-7.62" y="-5.08" drill="1" shape="octagon"/>
<pad name="12" x="-7.62" y="-2.54" drill="1" shape="octagon"/>
<pad name="13" x="-5.08" y="-5.08" drill="1" shape="octagon"/>
<pad name="14" x="-5.08" y="-2.54" drill="1" shape="octagon"/>
<pad name="15" x="-2.54" y="-5.08" drill="1" shape="octagon"/>
<pad name="16" x="-2.54" y="-2.54" drill="1" shape="octagon"/>
<pad name="17" x="0" y="-5.08" drill="1" shape="octagon"/>
<pad name="18" x="0" y="-2.54" drill="1" shape="octagon"/>
<pad name="19" x="2.54" y="-5.08" drill="1" shape="octagon"/>
<pad name="20" x="2.54" y="-2.54" drill="1" shape="octagon"/>
<pad name="21" x="5.08" y="-5.08" drill="1" shape="octagon"/>
<pad name="22" x="5.08" y="-2.54" drill="1" shape="octagon"/>
<pad name="23" x="7.62" y="-5.08" drill="1" shape="octagon"/>
<pad name="24" x="7.62" y="-2.54" drill="1" shape="octagon"/>
<pad name="25" x="10.16" y="-5.08" drill="1" shape="octagon"/>
<pad name="26" x="10.16" y="-2.54" drill="1" shape="octagon"/>
<pad name="27" x="12.7" y="-5.08" drill="1" shape="octagon"/>
<pad name="28" x="12.7" y="-2.54" drill="1" shape="octagon"/>
<pad name="29" x="15.24" y="-5.08" drill="1" shape="octagon"/>
<pad name="30" x="15.24" y="-2.54" drill="1" shape="octagon"/>
<pad name="31" x="17.78" y="-5.08" drill="1" shape="octagon"/>
<pad name="32" x="17.78" y="-2.54" drill="1" shape="octagon"/>
<pad name="33" x="20.32" y="-5.08" drill="1" shape="octagon"/>
<pad name="34" x="20.32" y="-2.54" drill="1" shape="octagon"/>
<text x="-20.32" y="-8.89" size="1.778" layer="25">&gt;NAME</text>
<text x="3.81" y="2.54" size="1.778" layer="27">&gt;VALUE</text>
<hole x="-28.72" y="3.66" drill="2.8"/>
<hole x="28.97" y="3.66" drill="2.8"/>
</package>
<package name="057-034-1">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
series 057 contact pc board low profile headers&lt;p&gt;
straight</description>
<wire x1="-1.9" y1="-3.32" x2="-1.9" y2="-4.03" width="0.2032" layer="21"/>
<wire x1="1.9" y1="-3.32" x2="1.9" y2="-4.03" width="0.2032" layer="21"/>
<wire x1="-22.74" y1="-1.97" x2="-22.04" y2="-3.04" width="0.4064" layer="21"/>
<wire x1="-22.04" y1="-3.04" x2="-21.34" y2="-1.97" width="0.4064" layer="21"/>
<wire x1="-21.34" y1="-1.97" x2="-22.74" y2="-1.97" width="0.4064" layer="21"/>
<wire x1="-25.45" y1="-4.1" x2="-25.45" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-25.45" y1="-4.1" x2="25.45" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="25.45" y1="-4.1" x2="25.45" y2="4.1" width="0.2032" layer="21"/>
<wire x1="25.45" y1="4.1" x2="-25.45" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-24.65" y1="-3.3" x2="-24.65" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-24.65" y1="3.3" x2="24.65" y2="3.3" width="0.2032" layer="21"/>
<wire x1="24.65" y1="3.3" x2="24.65" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="24.65" y1="-3.3" x2="1.9" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-1.9" y1="-3.3" x2="-24.65" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-25.45" y1="4.1" x2="-24.65" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-25.45" y1="-4.1" x2="-24.65" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="25.45" y1="4.1" x2="24.65" y2="3.3" width="0.2032" layer="21"/>
<wire x1="24.65" y1="-3.3" x2="25.45" y2="-4.1" width="0.2032" layer="21"/>
<pad name="1" x="-20.32" y="-1.27" drill="1" shape="octagon"/>
<pad name="2" x="-20.32" y="1.27" drill="1" shape="octagon"/>
<pad name="3" x="-17.78" y="-1.27" drill="1" shape="octagon"/>
<pad name="4" x="-17.78" y="1.27" drill="1" shape="octagon"/>
<pad name="5" x="-15.24" y="-1.27" drill="1" shape="octagon"/>
<pad name="6" x="-15.24" y="1.27" drill="1" shape="octagon"/>
<pad name="7" x="-12.7" y="-1.27" drill="1" shape="octagon"/>
<pad name="8" x="-12.7" y="1.27" drill="1" shape="octagon"/>
<pad name="9" x="-10.16" y="-1.27" drill="1" shape="octagon"/>
<pad name="10" x="-10.16" y="1.27" drill="1" shape="octagon"/>
<pad name="11" x="-7.62" y="-1.27" drill="1" shape="octagon"/>
<pad name="12" x="-7.62" y="1.27" drill="1" shape="octagon"/>
<pad name="13" x="-5.08" y="-1.27" drill="1" shape="octagon"/>
<pad name="14" x="-5.08" y="1.27" drill="1" shape="octagon"/>
<pad name="15" x="-2.54" y="-1.27" drill="1" shape="octagon"/>
<pad name="16" x="-2.54" y="1.27" drill="1" shape="octagon"/>
<pad name="17" x="0" y="-1.27" drill="1" shape="octagon"/>
<pad name="18" x="0" y="1.27" drill="1" shape="octagon"/>
<pad name="19" x="2.54" y="-1.27" drill="1" shape="octagon"/>
<pad name="20" x="2.54" y="1.27" drill="1" shape="octagon"/>
<pad name="21" x="5.08" y="-1.27" drill="1" shape="octagon"/>
<pad name="22" x="5.08" y="1.27" drill="1" shape="octagon"/>
<pad name="23" x="7.62" y="-1.27" drill="1" shape="octagon"/>
<pad name="24" x="7.62" y="1.27" drill="1" shape="octagon"/>
<pad name="25" x="10.16" y="-1.27" drill="1" shape="octagon"/>
<pad name="26" x="10.16" y="1.27" drill="1" shape="octagon"/>
<pad name="27" x="12.7" y="-1.27" drill="1" shape="octagon"/>
<pad name="28" x="12.7" y="1.27" drill="1" shape="octagon"/>
<pad name="29" x="15.24" y="-1.27" drill="1" shape="octagon"/>
<pad name="30" x="15.24" y="1.27" drill="1" shape="octagon"/>
<pad name="31" x="17.78" y="-1.27" drill="1" shape="octagon"/>
<pad name="32" x="17.78" y="1.27" drill="1" shape="octagon"/>
<pad name="33" x="20.32" y="-1.27" drill="1" shape="octagon"/>
<pad name="34" x="20.32" y="1.27" drill="1" shape="octagon"/>
<text x="-20.3" y="-6.88" size="1.778" layer="25">&gt;NAME</text>
<text x="-21.05" y="4.55" size="1.778" layer="27">&gt;VALUE</text>
</package>
<package name="057-010-0">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
series 057 contact pc board low profile headers&lt;p&gt;
angled</description>
<wire x1="-1.9" y1="-0.23" x2="-1.9" y2="5.86" width="0.2032" layer="21"/>
<wire x1="-1.9" y1="-0.23" x2="1.9" y2="-0.23" width="0.2032" layer="21"/>
<wire x1="1.9" y1="-0.23" x2="1.9" y2="5.86" width="0.2032" layer="21"/>
<wire x1="-5.8" y1="5.22" x2="-5.1" y2="3.25" width="0.4064" layer="21"/>
<wire x1="-5.1" y1="3.25" x2="-4.4" y2="5.22" width="0.4064" layer="21"/>
<wire x1="-4.4" y1="5.22" x2="-5.8" y2="5.22" width="0.4064" layer="21"/>
<wire x1="-10.21" y1="-2.54" x2="-10.21" y2="5.86" width="0.2032" layer="21"/>
<wire x1="10.21" y1="5.86" x2="10.21" y2="-2.44" width="0.2032" layer="21"/>
<wire x1="-10.16" y1="-2.54" x2="-7.62" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="-5.98" width="0.2032" layer="21"/>
<wire x1="10.16" y1="-2.54" x2="7.62" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="-6.05" width="0.2032" layer="21"/>
<wire x1="7.61" y1="-6.04" x2="-7.61" y2="-6.04" width="0.2032" layer="21"/>
<wire x1="-10.21" y1="5.86" x2="10.21" y2="5.86" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="-5.08" drill="1" shape="octagon"/>
<pad name="2" x="-5.08" y="-2.54" drill="1" shape="octagon"/>
<pad name="3" x="-2.54" y="-5.08" drill="1" shape="octagon"/>
<pad name="4" x="-2.54" y="-2.54" drill="1" shape="octagon"/>
<pad name="5" x="0" y="-5.08" drill="1" shape="octagon"/>
<pad name="6" x="0" y="-2.54" drill="1" shape="octagon"/>
<pad name="7" x="2.54" y="-5.08" drill="1" shape="octagon"/>
<pad name="8" x="2.54" y="-2.54" drill="1" shape="octagon"/>
<pad name="9" x="5.08" y="-5.08" drill="1" shape="octagon"/>
<pad name="10" x="5.08" y="-2.54" drill="1" shape="octagon"/>
<text x="-6.35" y="-8.89" size="1.778" layer="25">&gt;NAME</text>
<text x="-8.89" y="6.35" size="1.778" layer="27">&gt;VALUE</text>
<hole x="-13.48" y="3.66" drill="2.8"/>
<hole x="13.73" y="3.66" drill="2.8"/>
</package>
<package name="057-010-1">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
series 057 contact pc board low profile headers&lt;p&gt;
straight</description>
<wire x1="-1.9" y1="-3.32" x2="-1.9" y2="-4.03" width="0.2032" layer="21"/>
<wire x1="1.9" y1="-3.32" x2="1.9" y2="-4.03" width="0.2032" layer="21"/>
<wire x1="-7.5" y1="-1.97" x2="-6.8" y2="-3.04" width="0.4064" layer="21"/>
<wire x1="-6.8" y1="-3.04" x2="-6.1" y2="-1.97" width="0.4064" layer="21"/>
<wire x1="-6.1" y1="-1.97" x2="-7.5" y2="-1.97" width="0.4064" layer="21"/>
<wire x1="-10.21" y1="-4.1" x2="-10.21" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-10.21" y1="-4.1" x2="10.21" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="10.21" y1="-4.1" x2="10.21" y2="4.1" width="0.2032" layer="21"/>
<wire x1="10.21" y1="4.1" x2="-10.21" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-9.41" y1="-3.3" x2="-9.41" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-9.41" y1="3.3" x2="9.41" y2="3.3" width="0.2032" layer="21"/>
<wire x1="9.41" y1="3.3" x2="9.41" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="9.41" y1="-3.3" x2="1.9" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-1.9" y1="-3.3" x2="-9.41" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-10.21" y1="4.1" x2="-9.41" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-10.21" y1="-4.1" x2="-9.41" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="10.21" y1="4.1" x2="9.41" y2="3.3" width="0.2032" layer="21"/>
<wire x1="9.41" y1="-3.3" x2="10.21" y2="-4.1" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1" shape="octagon"/>
<pad name="2" x="-5.08" y="1.27" drill="1" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="1" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="1" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="1" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="1" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="1" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="1" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="1" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="1" shape="octagon"/>
<text x="-6.33" y="-6.88" size="1.778" layer="25">&gt;NAME</text>
<text x="-8.35" y="4.55" size="1.778" layer="27">&gt;VALUE</text>
</package>
<package name="057-006-0">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
series 057 contact pc board low profile headers&lt;p&gt;
angled</description>
<wire x1="-1.9" y1="-0.23" x2="-1.9" y2="5.86" width="0.2032" layer="21"/>
<wire x1="-1.9" y1="-0.23" x2="1.9" y2="-0.23" width="0.2032" layer="21"/>
<wire x1="1.9" y1="-0.23" x2="1.9" y2="5.86" width="0.2032" layer="21"/>
<wire x1="-5.8" y1="5.22" x2="-5.1" y2="3.25" width="0.4064" layer="21"/>
<wire x1="-5.1" y1="3.25" x2="-4.4" y2="5.22" width="0.4064" layer="21"/>
<wire x1="-4.4" y1="5.22" x2="-5.8" y2="5.22" width="0.4064" layer="21"/>
<wire x1="-7.67" y1="-2.54" x2="-7.67" y2="5.86" width="0.2032" layer="21"/>
<wire x1="7.67" y1="5.86" x2="7.67" y2="-2.44" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="-2.54" x2="-5.08" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="-5.98" width="0.2032" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="5.08" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-6.05" width="0.2032" layer="21"/>
<wire x1="5.07" y1="-6.04" x2="-5.07" y2="-6.04" width="0.2032" layer="21"/>
<wire x1="-7.67" y1="5.86" x2="7.67" y2="5.86" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="-5.08" drill="1" shape="octagon"/>
<pad name="2" x="-2.54" y="-2.54" drill="1" shape="octagon"/>
<pad name="3" x="0" y="-5.08" drill="1" shape="octagon"/>
<pad name="4" x="0" y="-2.54" drill="1" shape="octagon"/>
<pad name="5" x="2.54" y="-5.08" drill="1" shape="octagon"/>
<pad name="6" x="2.54" y="-2.54" drill="1" shape="octagon"/>
<text x="-5.08" y="-8.89" size="1.778" layer="25">&gt;NAME</text>
<text x="-6.35" y="6.35" size="1.778" layer="27">&gt;VALUE</text>
<hole x="-10.94" y="3.66" drill="2.8"/>
<hole x="11.19" y="3.66" drill="2.8"/>
</package>
<package name="057-006-1">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
series 057 contact pc board low profile headers&lt;p&gt;
straight</description>
<wire x1="-1.9" y1="-3.32" x2="-1.9" y2="-4.03" width="0.2032" layer="21"/>
<wire x1="1.9" y1="-3.32" x2="1.9" y2="-4.03" width="0.2032" layer="21"/>
<wire x1="-4.96" y1="-1.97" x2="-4.26" y2="-3.04" width="0.4064" layer="21"/>
<wire x1="-4.26" y1="-3.04" x2="-3.56" y2="-1.97" width="0.4064" layer="21"/>
<wire x1="-3.56" y1="-1.97" x2="-4.96" y2="-1.97" width="0.4064" layer="21"/>
<wire x1="-7.67" y1="-4.1" x2="-7.67" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-7.67" y1="-4.1" x2="7.67" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="7.67" y1="-4.1" x2="7.67" y2="4.1" width="0.2032" layer="21"/>
<wire x1="7.67" y1="4.1" x2="-7.67" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-6.87" y1="-3.3" x2="-6.87" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-6.87" y1="3.3" x2="6.87" y2="3.3" width="0.2032" layer="21"/>
<wire x1="6.87" y1="3.3" x2="6.87" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="6.87" y1="-3.3" x2="1.9" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-1.9" y1="-3.3" x2="-6.87" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-7.67" y1="4.1" x2="-6.87" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-7.67" y1="-4.1" x2="-6.87" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="7.67" y1="4.1" x2="6.87" y2="3.3" width="0.2032" layer="21"/>
<wire x1="6.87" y1="-3.3" x2="7.67" y2="-4.1" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1" shape="octagon"/>
<text x="-5.06" y="-6.88" size="1.778" layer="25">&gt;NAME</text>
<text x="-5.81" y="4.55" size="1.778" layer="27">&gt;VALUE</text>
</package>
<package name="057-014-0">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
series 057 contact pc board low profile headers&lt;p&gt;
angled</description>
<wire x1="-1.9" y1="-0.23" x2="-1.9" y2="5.86" width="0.2032" layer="21"/>
<wire x1="-1.9" y1="-0.23" x2="1.9" y2="-0.23" width="0.2032" layer="21"/>
<wire x1="1.9" y1="-0.23" x2="1.9" y2="5.86" width="0.2032" layer="21"/>
<wire x1="-8.34" y1="5.22" x2="-7.64" y2="3.25" width="0.4064" layer="21"/>
<wire x1="-7.64" y1="3.25" x2="-6.94" y2="5.22" width="0.4064" layer="21"/>
<wire x1="-6.94" y1="5.22" x2="-8.34" y2="5.22" width="0.4064" layer="21"/>
<wire x1="-12.75" y1="-2.54" x2="-12.75" y2="5.86" width="0.2032" layer="21"/>
<wire x1="12.75" y1="5.86" x2="12.75" y2="-2.44" width="0.2032" layer="21"/>
<wire x1="-12.7" y1="-2.54" x2="-10.16" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-10.16" y1="-2.54" x2="-10.16" y2="-5.98" width="0.2032" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="10.16" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="10.16" y1="-2.54" x2="10.16" y2="-6.05" width="0.2032" layer="21"/>
<wire x1="10.15" y1="-6.04" x2="-10.15" y2="-6.04" width="0.2032" layer="21"/>
<wire x1="-12.75" y1="5.86" x2="12.75" y2="5.86" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="-5.08" drill="1" shape="octagon"/>
<pad name="2" x="-7.62" y="-2.54" drill="1" shape="octagon"/>
<pad name="3" x="-5.08" y="-5.08" drill="1" shape="octagon"/>
<pad name="4" x="-5.08" y="-2.54" drill="1" shape="octagon"/>
<pad name="5" x="-2.54" y="-5.08" drill="1" shape="octagon"/>
<pad name="6" x="-2.54" y="-2.54" drill="1" shape="octagon"/>
<pad name="7" x="0" y="-5.08" drill="1" shape="octagon"/>
<pad name="8" x="0" y="-2.54" drill="1" shape="octagon"/>
<pad name="9" x="2.54" y="-5.08" drill="1" shape="octagon"/>
<pad name="10" x="2.54" y="-2.54" drill="1" shape="octagon"/>
<pad name="11" x="5.08" y="-5.08" drill="1" shape="octagon"/>
<pad name="12" x="5.08" y="-2.54" drill="1" shape="octagon"/>
<pad name="13" x="7.62" y="-5.08" drill="1" shape="octagon"/>
<pad name="14" x="7.62" y="-2.54" drill="1" shape="octagon"/>
<text x="-10.16" y="-8.89" size="1.778" layer="25">&gt;NAME</text>
<text x="-11.43" y="6.35" size="1.778" layer="27">&gt;VALUE</text>
<hole x="-16.02" y="3.66" drill="2.8"/>
<hole x="16.27" y="3.66" drill="2.8"/>
</package>
<package name="057-014-1">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
series 057 contact pc board low profile headers&lt;p&gt;
straight</description>
<wire x1="-1.9" y1="-3.32" x2="-1.9" y2="-4.03" width="0.2032" layer="21"/>
<wire x1="1.9" y1="-3.32" x2="1.9" y2="-4.03" width="0.2032" layer="21"/>
<wire x1="-10.04" y1="-1.97" x2="-9.34" y2="-3.04" width="0.4064" layer="21"/>
<wire x1="-9.34" y1="-3.04" x2="-8.64" y2="-1.97" width="0.4064" layer="21"/>
<wire x1="-8.64" y1="-1.97" x2="-10.04" y2="-1.97" width="0.4064" layer="21"/>
<wire x1="-12.75" y1="-4.1" x2="-12.75" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-12.75" y1="-4.1" x2="12.75" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="12.75" y1="-4.1" x2="12.75" y2="4.1" width="0.2032" layer="21"/>
<wire x1="12.75" y1="4.1" x2="-12.75" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-11.95" y1="-3.3" x2="-11.95" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-11.95" y1="3.3" x2="11.95" y2="3.3" width="0.2032" layer="21"/>
<wire x1="11.95" y1="3.3" x2="11.95" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="11.95" y1="-3.3" x2="1.9" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-1.9" y1="-3.3" x2="-11.95" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-12.75" y1="4.1" x2="-11.95" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-12.75" y1="-4.1" x2="-11.95" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="12.75" y1="4.1" x2="11.95" y2="3.3" width="0.2032" layer="21"/>
<wire x1="11.95" y1="-3.3" x2="12.75" y2="-4.1" width="0.2032" layer="21"/>
<pad name="1" x="-7.62" y="-1.27" drill="1" shape="octagon"/>
<pad name="2" x="-7.62" y="1.27" drill="1" shape="octagon"/>
<pad name="3" x="-5.08" y="-1.27" drill="1" shape="octagon"/>
<pad name="4" x="-5.08" y="1.27" drill="1" shape="octagon"/>
<pad name="5" x="-2.54" y="-1.27" drill="1" shape="octagon"/>
<pad name="6" x="-2.54" y="1.27" drill="1" shape="octagon"/>
<pad name="7" x="0" y="-1.27" drill="1" shape="octagon"/>
<pad name="8" x="0" y="1.27" drill="1" shape="octagon"/>
<pad name="9" x="2.54" y="-1.27" drill="1" shape="octagon"/>
<pad name="10" x="2.54" y="1.27" drill="1" shape="octagon"/>
<pad name="11" x="5.08" y="-1.27" drill="1" shape="octagon"/>
<pad name="12" x="5.08" y="1.27" drill="1" shape="octagon"/>
<pad name="13" x="7.62" y="-1.27" drill="1" shape="octagon"/>
<pad name="14" x="7.62" y="1.27" drill="1" shape="octagon"/>
<text x="-10.14" y="-6.88" size="1.778" layer="25">&gt;NAME</text>
<text x="-10.89" y="4.55" size="1.778" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="MV">
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-2.54" y="1.905" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M">
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="057-034-" prefix="X">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
34-pin series 057 contact pc board low profile headers</description>
<gates>
<gate name="-1" symbol="MV" x="-10.16" y="43.18" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="12.7" y="43.18" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="-10.16" y="38.1" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="12.7" y="38.1" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="-10.16" y="33.02" addlevel="always" swaplevel="1"/>
<gate name="-6" symbol="M" x="12.7" y="33.02" addlevel="always" swaplevel="1"/>
<gate name="-7" symbol="M" x="-10.16" y="27.94" addlevel="always" swaplevel="1"/>
<gate name="-8" symbol="M" x="12.7" y="27.94" addlevel="always" swaplevel="1"/>
<gate name="-9" symbol="M" x="-10.16" y="22.86" addlevel="always" swaplevel="1"/>
<gate name="-10" symbol="M" x="12.7" y="22.86" addlevel="always" swaplevel="1"/>
<gate name="-11" symbol="M" x="-10.16" y="17.78" addlevel="always" swaplevel="1"/>
<gate name="-12" symbol="M" x="12.7" y="17.78" addlevel="always" swaplevel="1"/>
<gate name="-13" symbol="M" x="-10.16" y="12.7" addlevel="always" swaplevel="1"/>
<gate name="-14" symbol="M" x="12.7" y="12.7" addlevel="always" swaplevel="1"/>
<gate name="-15" symbol="M" x="-10.16" y="7.62" addlevel="always" swaplevel="1"/>
<gate name="-16" symbol="M" x="12.7" y="7.62" addlevel="always" swaplevel="1"/>
<gate name="-17" symbol="M" x="-10.16" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-18" symbol="M" x="12.7" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-19" symbol="M" x="-10.16" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-20" symbol="M" x="12.7" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-21" symbol="M" x="-10.16" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-22" symbol="M" x="12.7" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-23" symbol="M" x="-10.16" y="-12.7" addlevel="always" swaplevel="1"/>
<gate name="-24" symbol="M" x="12.7" y="-12.7" addlevel="always" swaplevel="1"/>
<gate name="-25" symbol="M" x="-10.16" y="-17.78" addlevel="always" swaplevel="1"/>
<gate name="-26" symbol="M" x="12.7" y="-17.78" addlevel="always" swaplevel="1"/>
<gate name="-27" symbol="M" x="-10.16" y="-22.86" addlevel="always" swaplevel="1"/>
<gate name="-28" symbol="M" x="12.7" y="-22.86" addlevel="always" swaplevel="1"/>
<gate name="-29" symbol="M" x="-10.16" y="-27.94" addlevel="always" swaplevel="1"/>
<gate name="-30" symbol="M" x="12.7" y="-27.94" addlevel="always" swaplevel="1"/>
<gate name="-31" symbol="M" x="-10.16" y="-33.02" addlevel="always" swaplevel="1"/>
<gate name="-32" symbol="M" x="12.7" y="-33.02" addlevel="always" swaplevel="1"/>
<gate name="-33" symbol="M" x="-10.16" y="-38.1" addlevel="always" swaplevel="1"/>
<gate name="-34" symbol="M" x="12.7" y="-38.1" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="0" package="057-034-0">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-10" pin="S" pad="10"/>
<connect gate="-11" pin="S" pad="11"/>
<connect gate="-12" pin="S" pad="12"/>
<connect gate="-13" pin="S" pad="13"/>
<connect gate="-14" pin="S" pad="14"/>
<connect gate="-15" pin="S" pad="15"/>
<connect gate="-16" pin="S" pad="16"/>
<connect gate="-17" pin="S" pad="17"/>
<connect gate="-18" pin="S" pad="18"/>
<connect gate="-19" pin="S" pad="19"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-20" pin="S" pad="20"/>
<connect gate="-21" pin="S" pad="21"/>
<connect gate="-22" pin="S" pad="22"/>
<connect gate="-23" pin="S" pad="23"/>
<connect gate="-24" pin="S" pad="24"/>
<connect gate="-25" pin="S" pad="25"/>
<connect gate="-26" pin="S" pad="26"/>
<connect gate="-27" pin="S" pad="27"/>
<connect gate="-28" pin="S" pad="28"/>
<connect gate="-29" pin="S" pad="29"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-30" pin="S" pad="30"/>
<connect gate="-31" pin="S" pad="31"/>
<connect gate="-32" pin="S" pad="32"/>
<connect gate="-33" pin="S" pad="33"/>
<connect gate="-34" pin="S" pad="34"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
<connect gate="-9" pin="S" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="1" package="057-034-1">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-10" pin="S" pad="10"/>
<connect gate="-11" pin="S" pad="11"/>
<connect gate="-12" pin="S" pad="12"/>
<connect gate="-13" pin="S" pad="13"/>
<connect gate="-14" pin="S" pad="14"/>
<connect gate="-15" pin="S" pad="15"/>
<connect gate="-16" pin="S" pad="16"/>
<connect gate="-17" pin="S" pad="17"/>
<connect gate="-18" pin="S" pad="18"/>
<connect gate="-19" pin="S" pad="19"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-20" pin="S" pad="20"/>
<connect gate="-21" pin="S" pad="21"/>
<connect gate="-22" pin="S" pad="22"/>
<connect gate="-23" pin="S" pad="23"/>
<connect gate="-24" pin="S" pad="24"/>
<connect gate="-25" pin="S" pad="25"/>
<connect gate="-26" pin="S" pad="26"/>
<connect gate="-27" pin="S" pad="27"/>
<connect gate="-28" pin="S" pad="28"/>
<connect gate="-29" pin="S" pad="29"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-30" pin="S" pad="30"/>
<connect gate="-31" pin="S" pad="31"/>
<connect gate="-32" pin="S" pad="32"/>
<connect gate="-33" pin="S" pad="33"/>
<connect gate="-34" pin="S" pad="34"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
<connect gate="-9" pin="S" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="057-010-" prefix="X">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
10-pin series 057 contact pc board low profile headers</description>
<gates>
<gate name="-1" symbol="MV" x="-10.16" y="43.18" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="12.7" y="43.18" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="-10.16" y="38.1" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="12.7" y="38.1" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="-10.16" y="33.02" addlevel="always" swaplevel="1"/>
<gate name="-6" symbol="M" x="12.7" y="33.02" addlevel="always" swaplevel="1"/>
<gate name="-7" symbol="M" x="-10.16" y="27.94" addlevel="always" swaplevel="1"/>
<gate name="-8" symbol="M" x="12.7" y="27.94" addlevel="always" swaplevel="1"/>
<gate name="-9" symbol="M" x="-10.16" y="22.86" addlevel="always" swaplevel="1"/>
<gate name="-10" symbol="M" x="12.7" y="22.86" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="0" package="057-010-0">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-10" pin="S" pad="10"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
<connect gate="-9" pin="S" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="1" package="057-010-1">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-10" pin="S" pad="10"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
<connect gate="-9" pin="S" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="057-006-" prefix="X">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
6-pin series 057 contact pc board low profile headers</description>
<gates>
<gate name="-1" symbol="MV" x="-10.16" y="43.18" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="12.7" y="43.18" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="-10.16" y="38.1" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="12.7" y="38.1" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="-10.16" y="33.02" addlevel="always" swaplevel="1"/>
<gate name="-6" symbol="M" x="12.7" y="33.02" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="0" package="057-006-0">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="1" package="057-006-1">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="057-014-" prefix="X">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
10-pin series 057 contact pc board low profile headers</description>
<gates>
<gate name="-1" symbol="MV" x="-10.16" y="43.18" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="12.7" y="43.18" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="-10.16" y="38.1" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="12.7" y="38.1" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="-10.16" y="33.02" addlevel="always" swaplevel="1"/>
<gate name="-6" symbol="M" x="12.7" y="33.02" addlevel="always" swaplevel="1"/>
<gate name="-7" symbol="M" x="-10.16" y="27.94" addlevel="always" swaplevel="1"/>
<gate name="-8" symbol="M" x="12.7" y="27.94" addlevel="always" swaplevel="1"/>
<gate name="-9" symbol="M" x="-10.16" y="22.86" addlevel="always" swaplevel="1"/>
<gate name="-10" symbol="M" x="12.7" y="22.86" addlevel="always" swaplevel="1"/>
<gate name="-11" symbol="M" x="-10.16" y="17.78" addlevel="always" swaplevel="1"/>
<gate name="-12" symbol="M" x="12.7" y="17.78" addlevel="always" swaplevel="1"/>
<gate name="-13" symbol="M" x="-10.16" y="12.7" addlevel="always" swaplevel="1"/>
<gate name="-14" symbol="M" x="12.7" y="12.7" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="0" package="057-014-0">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-10" pin="S" pad="10"/>
<connect gate="-11" pin="S" pad="11"/>
<connect gate="-12" pin="S" pad="12"/>
<connect gate="-13" pin="S" pad="13"/>
<connect gate="-14" pin="S" pad="14"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
<connect gate="-9" pin="S" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="1" package="057-014-1">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-10" pin="S" pad="10"/>
<connect gate="-11" pin="S" pad="11"/>
<connect gate="-12" pin="S" pad="12"/>
<connect gate="-13" pin="S" pad="13"/>
<connect gate="-14" pin="S" pad="14"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
<connect gate="-9" pin="S" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="isenproject">
<description>ISENproject : bibliothèque de composants utilisable pour les projets des CSI3, CIR3, M1 et M2


created by severine.corvez@isen.fr</description>
<packages>
<package name="R">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.9" diameter="2.1844" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.9" diameter="2.1844" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="KK-156-2">
<description>&lt;b&gt;KK 156 HEADER&lt;/b&gt;&lt;p&gt;
Source: http://www.molex.com/pdm_docs/sd/026604100_sd.pdf</description>
<wire x1="3.81" y1="4.95" x2="2.01" y2="4.95" width="0.2032" layer="21"/>
<wire x1="2.01" y1="4.95" x2="-1.985" y2="4.95" width="0.2032" layer="21"/>
<wire x1="-1.985" y1="4.95" x2="-3.785" y2="4.95" width="0.2032" layer="21"/>
<wire x1="-3.785" y1="4.95" x2="-3.785" y2="-4.825" width="0.2032" layer="21"/>
<wire x1="-3.785" y1="-4.825" x2="3.81" y2="-4.825" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-4.825" x2="3.81" y2="4.95" width="0.2032" layer="21"/>
<wire x1="-1.985" y1="2.525" x2="2.01" y2="2.525" width="0.2032" layer="21"/>
<wire x1="2.01" y1="2.525" x2="2.01" y2="4.95" width="0.2032" layer="21"/>
<wire x1="-1.985" y1="2.525" x2="-1.985" y2="4.95" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1" diameter="2.1844" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1" diameter="2.1844" shape="long" rot="R90"/>
<text x="-4.48" y="-4.445" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.75" y="-4.445" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+5V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="MV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="1/4W" package="R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+5V" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KK-156-2" prefix="X" uservalue="yes">
<description>&lt;b&gt;KK 156 HEADER&lt;/b&gt;&lt;p&gt;
Source: http://www.molex.com/pdm_docs/sd/026604100_sd.pdf</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="MV" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="KK-156-2">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-amp">
<description>&lt;b&gt;AMP Connectors&lt;/b&gt;&lt;p&gt;
RJ45 Jack connectors&lt;br&gt;
 Based on the previous libraris:
 &lt;ul&gt;
 &lt;li&gt;amp.lbr
 &lt;li&gt;amp-j.lbr
 &lt;li&gt;amp-mta.lbr
 &lt;li&gt;amp-nlok.lbr
 &lt;li&gt;amp-sim.lbr
 &lt;li&gt;amp-micro-match.lbr
 &lt;/ul&gt;
 Sources :
 &lt;ul&gt;
 &lt;li&gt;Catalog 82066 Revised 11-95 
 &lt;li&gt;Product Guide 296785 Rev. 8-99
 &lt;li&gt;Product Guide CD-ROM 1999
 &lt;li&gt;www.amp.com
 &lt;/ul&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="10X08MTA">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="-10.16" y1="-1.27" x2="-10.16" y2="1.27" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.27" x2="-10.16" y2="1.27" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.27" x2="10.16" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.27" x2="10.16" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.27" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="1.27" width="0.1524" layer="21"/>
<pad name="7" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="8.89" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="-8.89" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.9812" y="-3.2512" size="1.27" layer="25">&gt;NAME</text>
<text x="-10.0762" y="2.2507" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="21"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="21"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="21"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="21"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="21"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="21"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="21"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="21"/>
</package>
<package name="10X09MTA">
<description>&lt;b&gt;AMP MTA connector&lt;/b&gt;&lt;p&gt;
Source: http://ecommas.tycoelectronics.com .. ENG_CD_640456_W.pdf</description>
<wire x1="-11.43" y1="-1.27" x2="-11.43" y2="1.27" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.27" x2="-11.43" y2="1.27" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.27" x2="11.43" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-1.27" x2="11.43" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="1.27" x2="-11.43" y2="1.905" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.905" x2="-11.43" y2="1.905" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.905" x2="11.43" y2="1.27" width="0.1524" layer="21"/>
<pad name="7" x="-5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="10.16" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="-7.62" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="-10.16" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="-3.2512" size="1.27" layer="25">&gt;NAME</text>
<text x="-11.1763" y="2.1509" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="21"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="21"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="21"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="21"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="21"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="21"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="21"/>
<rectangle x1="-7.874" y1="-0.254" x2="-7.366" y2="0.254" layer="21"/>
<rectangle x1="-10.414" y1="-0.254" x2="-9.906" y2="0.254" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MTA-1_8">
<wire x1="-8.89" y1="1.27" x2="-8.89" y2="-1.905" width="0.254" layer="94"/>
<wire x1="11.43" y1="-1.905" x2="-8.89" y2="-1.905" width="0.254" layer="94"/>
<wire x1="11.43" y1="-1.905" x2="11.43" y2="1.27" width="0.254" layer="94"/>
<wire x1="-8.89" y1="1.27" x2="11.43" y2="1.27" width="0.254" layer="94"/>
<circle x="-7.62" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="-5.08" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="5.08" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="7.62" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="10.16" y="0" radius="0.635" width="0.254" layer="94"/>
<text x="12.7" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="12.7" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="-8.89" y="0" size="1.27" layer="95" rot="R180">1</text>
<pin name="1" x="-7.62" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="2" x="-5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="3" x="-2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="4" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="5" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="6" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="7" x="7.62" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="8" x="10.16" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="MTA-1_9">
<wire x1="-11.43" y1="1.27" x2="-11.43" y2="-1.905" width="0.254" layer="94"/>
<wire x1="11.43" y1="-1.905" x2="-11.43" y2="-1.905" width="0.254" layer="94"/>
<wire x1="11.43" y1="-1.905" x2="11.43" y2="1.27" width="0.254" layer="94"/>
<wire x1="-11.43" y1="1.27" x2="11.43" y2="1.27" width="0.254" layer="94"/>
<circle x="-10.16" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="-7.62" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="-5.08" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="5.08" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="7.62" y="0" radius="0.635" width="0.254" layer="94"/>
<circle x="10.16" y="0" radius="0.635" width="0.254" layer="94"/>
<text x="12.7" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="-11.43" y="0" size="1.27" layer="95" rot="R180">1</text>
<text x="12.7" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-10.16" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="2" x="-7.62" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="3" x="-5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="4" x="-2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="6" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="7" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="8" x="7.62" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="9" x="10.16" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MTA08-100" prefix="J" uservalue="yes">
<description>&lt;b&gt;AMP connector&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MTA-1_8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="10X08MTA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MTA09-100" prefix="J" uservalue="yes">
<description>&lt;b&gt;AMP connector&lt;/b&gt;</description>
<gates>
<gate name="G$2" symbol="MTA-1_9" x="0" y="0"/>
</gates>
<devices>
<device name="" package="10X09MTA">
<connects>
<connect gate="G$2" pin="1" pad="1"/>
<connect gate="G$2" pin="2" pad="2"/>
<connect gate="G$2" pin="3" pad="3"/>
<connect gate="G$2" pin="4" pad="4"/>
<connect gate="G$2" pin="5" pad="5"/>
<connect gate="G$2" pin="6" pad="6"/>
<connect gate="G$2" pin="7" pad="7"/>
<connect gate="G$2" pin="8" pad="8"/>
<connect gate="G$2" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-garry" urn="urn:adsk.eagle:library:147">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;a href="www.mpe-connector.de"&gt;Menufacturer&lt;/a&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="332-02" urn="urn:adsk.eagle:footprint:6784/1" library_version="1">
<description>&lt;b&gt;2 Pin - 2mm Dual Row&lt;/b&gt;&lt;p&gt;
Source: www.mpe-connector.de / garry_shortform_2012.pdf</description>
<wire x1="-0.85" y1="-1.9" x2="0.85" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="0.85" y1="-1.9" x2="0.85" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="0.85" y1="0.4" x2="0.85" y2="1.9" width="0.2032" layer="21"/>
<wire x1="0.85" y1="1.9" x2="-0.85" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-0.85" y1="1.9" x2="-0.85" y2="0.4" width="0.2032" layer="21"/>
<wire x1="-0.85" y1="-0.4" x2="-0.85" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-0.85" y1="0.4" x2="-0.85" y2="-0.4" width="0.2032" layer="21" curve="-129.185"/>
<wire x1="0.85" y1="-0.4" x2="0.85" y2="0.4" width="0.2032" layer="21" curve="-129.185"/>
<pad name="1" x="0" y="-1" drill="0.9" diameter="1.27"/>
<pad name="2" x="0" y="1" drill="0.9" diameter="1.27"/>
<text x="-0.65" y="-1.75" size="0.3048" layer="21" font="vector">1</text>
<text x="-0.62" y="-3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.62" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="-0.75" layer="51"/>
<rectangle x1="-0.25" y1="0.75" x2="0.25" y2="1.25" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="332-02" urn="urn:adsk.eagle:package:6810/1" type="box" library_version="1">
<description>2 Pin - 2mm Dual Row
Source: www.mpe-connector.de / garry_shortform_2012.pdf</description>
<packageinstances>
<packageinstance name="332-02"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MV" urn="urn:adsk.eagle:symbol:6783/1" library_version="1">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="332-02" urn="urn:adsk.eagle:component:6832/1" prefix="X" library_version="1">
<description>&lt;b&gt;2 Pin - 2mm Dual Row&lt;/b&gt;&lt;p&gt;
Source: www.mpe-connector.de / garry_shortform_2012.pdf</description>
<gates>
<gate name="-1" symbol="MV" x="-10.16" y="0" addlevel="always"/>
<gate name="-2" symbol="MV" x="10.16" y="0" addlevel="always"/>
</gates>
<devices>
<device name="" package="332-02">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6810/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="X5" library="con-panduit" deviceset="057-034-" device="1"/>
<part name="X1" library="con-panduit" deviceset="057-034-" device="1"/>
<part name="R1" library="isenproject" deviceset="R" device="1/4W"/>
<part name="R2" library="isenproject" deviceset="R" device="1/4W"/>
<part name="SUPPLY2" library="isenproject" deviceset="+5V" device=""/>
<part name="X6" library="isenproject" deviceset="KK-156-2" device=""/>
<part name="SUPPLY7" library="isenproject" deviceset="GND" device=""/>
<part name="SUPPLY8" library="isenproject" deviceset="GND" device=""/>
<part name="SUPPLY10" library="isenproject" deviceset="+5V" device=""/>
<part name="SUPPLY11" library="isenproject" deviceset="GND" device=""/>
<part name="SUPPLY12" library="isenproject" deviceset="+5V" device=""/>
<part name="SUPPLY13" library="isenproject" deviceset="GND" device=""/>
<part name="SUPPLY17" library="isenproject" deviceset="+5V" device=""/>
<part name="SUPPLY18" library="isenproject" deviceset="GND" device=""/>
<part name="X7" library="con-panduit" deviceset="057-010-" device="1"/>
<part name="X11" library="con-panduit" deviceset="057-006-" device="1"/>
<part name="X12" library="con-panduit" deviceset="057-006-" device="1"/>
<part name="X13" library="con-panduit" deviceset="057-014-" device="1"/>
<part name="X15" library="con-panduit" deviceset="057-006-" device="1"/>
<part name="SUPPLY1" library="isenproject" deviceset="GND" device=""/>
<part name="SUPPLY5" library="isenproject" deviceset="GND" device=""/>
<part name="SUPPLY9" library="isenproject" deviceset="+5V" device=""/>
<part name="SUPPLY3" library="isenproject" deviceset="+5V" device=""/>
<part name="SUPPLY4" library="isenproject" deviceset="+5V" device=""/>
<part name="SUPPLY6" library="isenproject" deviceset="+5V" device=""/>
<part name="X2" library="con-panduit" deviceset="057-006-" device="1"/>
<part name="X3" library="con-panduit" deviceset="057-010-" device="1"/>
<part name="SUPPLY14" library="isenproject" deviceset="GND" device=""/>
<part name="SUPPLY15" library="isenproject" deviceset="+5V" device=""/>
<part name="SUPPLY16" library="isenproject" deviceset="+5V" device=""/>
<part name="J2" library="con-amp" deviceset="MTA08-100" device=""/>
<part name="J3" library="con-amp" deviceset="MTA08-100" device=""/>
<part name="J4" library="con-amp" deviceset="MTA09-100" device=""/>
<part name="J5" library="con-amp" deviceset="MTA09-100" device=""/>
<part name="J6" library="con-amp" deviceset="MTA08-100" device=""/>
<part name="J7" library="con-amp" deviceset="MTA08-100" device=""/>
<part name="J8" library="con-amp" deviceset="MTA09-100" device=""/>
<part name="J9" library="con-amp" deviceset="MTA09-100" device=""/>
<part name="X4" library="con-garry" library_urn="urn:adsk.eagle:library:147" deviceset="332-02" device="" package3d_urn="urn:adsk.eagle:package:6810/1"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="215.9" y="91.44" size="3.81" layer="91">Roue</text>
<text x="177.8" y="27.94" size="3.81" layer="91" rot="MR0">Suiveur</text>
<text x="-50.8" y="119.38" size="3.81" layer="91" rot="R180">PORT A</text>
<text x="88.9" y="116.84" size="3.81" layer="91" rot="R180">PORT B</text>
<text x="43.18" y="185.42" size="3.81" layer="91" rot="R180">LANCEUR</text>
<text x="198.12" y="193.04" size="3.81" layer="91" rot="R180">Trieur</text>
<text x="35.56" y="0" size="3.81" layer="91">ALIM +5V</text>
<text x="215.9" y="27.94" size="3.81" layer="91" rot="MR0">Bras</text>
<text x="81.28" y="203.2" size="3.81" layer="91" rot="R180">AJOUTER UNE SORTIE VERS LE PORT C</text>
<text x="-63.5" y="182.88" size="3.81" layer="91" rot="R180">Lidar</text>
</plain>
<instances>
<instance part="X5" gate="-1" x="-33.02" y="27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="28.702" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="-30.48" y="26.035" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="X5" gate="-2" x="-86.36" y="27.94" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="28.702" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-3" x="-33.02" y="33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="33.782" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-4" x="-86.36" y="33.02" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="33.782" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-5" x="-33.02" y="38.1" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="38.862" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-6" x="-86.36" y="38.1" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="38.862" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-7" x="-33.02" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="43.942" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-8" x="-86.36" y="43.18" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="43.942" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-9" x="-33.02" y="48.26" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="49.022" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-10" x="-86.36" y="48.26" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="49.022" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-11" x="-33.02" y="53.34" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="54.102" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-12" x="-86.36" y="53.34" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="54.102" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-13" x="-33.02" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="59.182" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-14" x="-86.36" y="58.42" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="59.182" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-15" x="-33.02" y="63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="64.262" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-16" x="-86.36" y="63.5" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="64.262" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-17" x="-33.02" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="69.342" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-18" x="-86.36" y="68.58" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="69.342" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-19" x="-33.02" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="74.422" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-20" x="-86.36" y="73.66" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="74.422" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-21" x="-33.02" y="78.74" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="79.502" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-22" x="-86.36" y="78.74" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="79.502" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-23" x="-33.02" y="83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="84.582" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-24" x="-86.36" y="83.82" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="84.582" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-25" x="-33.02" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="89.662" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-26" x="-86.36" y="88.9" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="89.662" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-27" x="-33.02" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="94.742" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-28" x="-86.36" y="93.98" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="94.742" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-29" x="-33.02" y="99.06" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="99.822" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-30" x="-86.36" y="99.06" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="99.822" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-31" x="-33.02" y="104.14" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="104.902" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-32" x="-86.36" y="104.14" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="104.902" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X5" gate="-33" x="-33.02" y="109.22" smashed="yes" rot="R180">
<attribute name="NAME" x="-34.036" y="109.982" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-34" x="-86.36" y="109.22" smashed="yes" rot="MR180">
<attribute name="NAME" x="-85.344" y="109.982" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X1" gate="-1" x="53.34" y="106.68" smashed="yes">
<attribute name="NAME" x="54.356" y="105.918" size="1.524" layer="95"/>
<attribute name="VALUE" x="50.8" y="108.585" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="-2" x="106.68" y="106.68" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="105.918" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-3" x="53.34" y="101.6" smashed="yes">
<attribute name="NAME" x="54.356" y="100.838" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-4" x="106.68" y="101.6" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="100.838" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-5" x="53.34" y="96.52" smashed="yes">
<attribute name="NAME" x="54.356" y="95.758" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-6" x="106.68" y="96.52" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="95.758" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-7" x="53.34" y="91.44" smashed="yes">
<attribute name="NAME" x="54.356" y="90.678" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-8" x="106.68" y="91.44" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="90.678" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-9" x="53.34" y="86.36" smashed="yes">
<attribute name="NAME" x="54.356" y="85.598" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-10" x="106.68" y="86.36" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="85.598" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-11" x="53.34" y="81.28" smashed="yes">
<attribute name="NAME" x="54.356" y="80.518" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-12" x="106.68" y="81.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="80.518" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-13" x="53.34" y="76.2" smashed="yes">
<attribute name="NAME" x="54.356" y="75.438" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-14" x="106.68" y="76.2" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="75.438" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-15" x="53.34" y="71.12" smashed="yes">
<attribute name="NAME" x="54.356" y="70.358" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-16" x="106.68" y="71.12" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="70.358" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-17" x="53.34" y="66.04" smashed="yes">
<attribute name="NAME" x="54.356" y="65.278" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-18" x="106.68" y="66.04" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="65.278" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-19" x="53.34" y="60.96" smashed="yes">
<attribute name="NAME" x="54.356" y="60.198" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-20" x="106.68" y="60.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="60.198" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-21" x="53.34" y="55.88" smashed="yes">
<attribute name="NAME" x="54.356" y="55.118" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-22" x="106.68" y="55.88" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="55.118" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-23" x="53.34" y="50.8" smashed="yes">
<attribute name="NAME" x="54.356" y="50.038" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-24" x="106.68" y="50.8" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="50.038" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-25" x="53.34" y="45.72" smashed="yes">
<attribute name="NAME" x="54.356" y="44.958" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-26" x="106.68" y="45.72" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="44.958" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-27" x="53.34" y="40.64" smashed="yes">
<attribute name="NAME" x="54.356" y="39.878" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-28" x="106.68" y="40.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="39.878" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-29" x="53.34" y="35.56" smashed="yes">
<attribute name="NAME" x="54.356" y="34.798" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-30" x="106.68" y="35.56" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="34.798" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-31" x="53.34" y="30.48" smashed="yes">
<attribute name="NAME" x="54.356" y="29.718" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-32" x="106.68" y="30.48" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="29.718" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-33" x="53.34" y="25.4" smashed="yes">
<attribute name="NAME" x="54.356" y="24.638" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-34" x="106.68" y="25.4" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.664" y="24.638" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="R1" gate="G$1" x="218.44" y="121.92" smashed="yes" rot="R180">
<attribute name="NAME" x="222.25" y="120.4214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="222.25" y="125.222" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R2" gate="G$1" x="218.44" y="132.08" smashed="yes" rot="MR180">
<attribute name="NAME" x="214.63" y="130.5814" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="214.63" y="135.382" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="SUPPLY2" gate="+5V" x="185.42" y="167.64" smashed="yes">
<attribute name="VALUE" x="183.515" y="170.815" size="1.778" layer="96"/>
</instance>
<instance part="X6" gate="-1" x="45.72" y="-12.7" smashed="yes">
<attribute name="NAME" x="48.26" y="-13.462" size="1.524" layer="95"/>
<attribute name="VALUE" x="44.958" y="-11.303" size="1.778" layer="96"/>
</instance>
<instance part="X6" gate="-2" x="45.72" y="-17.78" smashed="yes">
<attribute name="NAME" x="48.26" y="-18.542" size="1.524" layer="95"/>
<attribute name="VALUE" x="44.958" y="-16.383" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY7" gate="GND" x="2.54" y="-20.32" smashed="yes">
<attribute name="VALUE" x="0.635" y="-23.495" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY8" gate="GND" x="129.54" y="58.42" smashed="yes">
<attribute name="VALUE" x="127.635" y="55.245" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY10" gate="+5V" x="238.76" y="7.62" smashed="yes" rot="MR0">
<attribute name="VALUE" x="240.665" y="10.795" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="SUPPLY11" gate="GND" x="195.58" y="-15.24" smashed="yes">
<attribute name="VALUE" x="193.675" y="-18.415" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY12" gate="+5V" x="-2.54" y="170.18" smashed="yes">
<attribute name="VALUE" x="-4.445" y="173.355" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY13" gate="GND" x="22.86" y="144.78" smashed="yes">
<attribute name="VALUE" x="20.955" y="141.605" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY17" gate="+5V" x="251.46" y="86.36" smashed="yes">
<attribute name="VALUE" x="249.555" y="89.535" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY18" gate="GND" x="251.46" y="50.8" smashed="yes">
<attribute name="VALUE" x="249.555" y="47.625" size="1.778" layer="96"/>
</instance>
<instance part="X7" gate="-1" x="203.2" y="15.24" smashed="yes" rot="MR0">
<attribute name="NAME" x="202.184" y="14.478" size="1.524" layer="95" rot="MR0"/>
<attribute name="VALUE" x="205.74" y="17.145" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="X7" gate="-2" x="185.42" y="15.24" smashed="yes">
<attribute name="NAME" x="186.436" y="14.478" size="1.524" layer="95"/>
</instance>
<instance part="X7" gate="-3" x="203.2" y="10.16" smashed="yes" rot="MR0">
<attribute name="NAME" x="202.184" y="9.398" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X7" gate="-4" x="185.42" y="10.16" smashed="yes">
<attribute name="NAME" x="186.436" y="9.398" size="1.524" layer="95"/>
</instance>
<instance part="X7" gate="-5" x="203.2" y="5.08" smashed="yes" rot="MR0">
<attribute name="NAME" x="202.184" y="4.318" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X7" gate="-6" x="185.42" y="5.08" smashed="yes">
<attribute name="NAME" x="186.436" y="4.318" size="1.524" layer="95"/>
</instance>
<instance part="X7" gate="-7" x="203.2" y="0" smashed="yes" rot="MR0">
<attribute name="NAME" x="202.184" y="-0.762" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X7" gate="-8" x="185.42" y="0" smashed="yes">
<attribute name="NAME" x="186.436" y="-0.762" size="1.524" layer="95"/>
</instance>
<instance part="X7" gate="-9" x="203.2" y="-5.08" smashed="yes" rot="MR0">
<attribute name="NAME" x="202.184" y="-5.842" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X7" gate="-10" x="185.42" y="-5.08" smashed="yes">
<attribute name="NAME" x="186.436" y="-5.842" size="1.524" layer="95"/>
</instance>
<instance part="X11" gate="-1" x="144.78" y="167.64" smashed="yes">
<attribute name="NAME" x="145.796" y="166.878" size="1.524" layer="95"/>
<attribute name="VALUE" x="142.24" y="169.545" size="1.778" layer="96"/>
</instance>
<instance part="X11" gate="-2" x="162.56" y="167.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="161.544" y="166.878" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X11" gate="-3" x="144.78" y="162.56" smashed="yes">
<attribute name="NAME" x="145.796" y="161.798" size="1.524" layer="95"/>
</instance>
<instance part="X11" gate="-4" x="162.56" y="162.56" smashed="yes" rot="MR0">
<attribute name="NAME" x="161.544" y="161.798" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X11" gate="-5" x="144.78" y="157.48" smashed="yes">
<attribute name="NAME" x="145.796" y="156.718" size="1.524" layer="95"/>
</instance>
<instance part="X11" gate="-6" x="162.56" y="157.48" smashed="yes" rot="MR0">
<attribute name="NAME" x="161.544" y="156.718" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X12" gate="-1" x="208.28" y="167.64" smashed="yes">
<attribute name="NAME" x="209.296" y="166.878" size="1.524" layer="95"/>
<attribute name="VALUE" x="205.74" y="169.545" size="1.778" layer="96"/>
</instance>
<instance part="X12" gate="-2" x="226.06" y="167.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="225.044" y="166.878" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X12" gate="-3" x="208.28" y="162.56" smashed="yes">
<attribute name="NAME" x="209.296" y="161.798" size="1.524" layer="95"/>
</instance>
<instance part="X12" gate="-4" x="226.06" y="162.56" smashed="yes" rot="MR0">
<attribute name="NAME" x="225.044" y="161.798" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X12" gate="-5" x="208.28" y="157.48" smashed="yes">
<attribute name="NAME" x="209.296" y="156.718" size="1.524" layer="95"/>
</instance>
<instance part="X12" gate="-6" x="226.06" y="157.48" smashed="yes" rot="MR0">
<attribute name="NAME" x="225.044" y="156.718" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X13" gate="-1" x="208.28" y="83.82" smashed="yes">
<attribute name="NAME" x="209.296" y="83.058" size="1.524" layer="95"/>
<attribute name="VALUE" x="205.74" y="85.725" size="1.778" layer="96"/>
</instance>
<instance part="X13" gate="-2" x="228.6" y="83.82" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.584" y="83.058" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X13" gate="-3" x="208.28" y="78.74" smashed="yes">
<attribute name="NAME" x="209.296" y="77.978" size="1.524" layer="95"/>
</instance>
<instance part="X13" gate="-4" x="228.6" y="78.74" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.584" y="77.978" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X13" gate="-5" x="208.28" y="73.66" smashed="yes">
<attribute name="NAME" x="209.296" y="72.898" size="1.524" layer="95"/>
</instance>
<instance part="X13" gate="-6" x="228.6" y="73.66" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.584" y="72.898" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X13" gate="-7" x="208.28" y="68.58" smashed="yes">
<attribute name="NAME" x="209.296" y="67.818" size="1.524" layer="95"/>
</instance>
<instance part="X13" gate="-8" x="228.6" y="68.58" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.584" y="67.818" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X13" gate="-9" x="208.28" y="63.5" smashed="yes">
<attribute name="NAME" x="209.296" y="62.738" size="1.524" layer="95"/>
</instance>
<instance part="X13" gate="-10" x="228.6" y="63.5" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.584" y="62.738" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X13" gate="-11" x="208.28" y="58.42" smashed="yes">
<attribute name="NAME" x="209.296" y="57.658" size="1.524" layer="95"/>
</instance>
<instance part="X13" gate="-12" x="228.6" y="58.42" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.584" y="57.658" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X13" gate="-13" x="208.28" y="53.34" smashed="yes">
<attribute name="NAME" x="209.296" y="52.578" size="1.524" layer="95"/>
</instance>
<instance part="X13" gate="-14" x="228.6" y="53.34" smashed="yes" rot="MR0">
<attribute name="NAME" x="227.584" y="52.578" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X15" gate="-1" x="20.32" y="170.18" smashed="yes">
<attribute name="NAME" x="21.336" y="169.418" size="1.524" layer="95"/>
<attribute name="VALUE" x="17.78" y="172.085" size="1.778" layer="96"/>
</instance>
<instance part="X15" gate="-2" x="43.18" y="170.18" smashed="yes" rot="MR0">
<attribute name="NAME" x="42.164" y="169.418" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X15" gate="-3" x="20.32" y="165.1" smashed="yes">
<attribute name="NAME" x="21.336" y="164.338" size="1.524" layer="95"/>
</instance>
<instance part="X15" gate="-4" x="43.18" y="165.1" smashed="yes" rot="MR0">
<attribute name="NAME" x="42.164" y="164.338" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X15" gate="-5" x="20.32" y="160.02" smashed="yes">
<attribute name="NAME" x="21.336" y="159.258" size="1.524" layer="95"/>
</instance>
<instance part="X15" gate="-6" x="43.18" y="160.02" smashed="yes" rot="MR0">
<attribute name="NAME" x="42.164" y="159.258" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="-106.68" y="96.52" smashed="yes">
<attribute name="VALUE" x="-108.585" y="93.345" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="154.94" y="182.88" smashed="yes" rot="R180">
<attribute name="VALUE" x="156.845" y="186.055" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY9" gate="+5V" x="121.92" y="167.64" smashed="yes">
<attribute name="VALUE" x="120.015" y="170.815" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY3" gate="+5V" x="147.32" y="7.62" smashed="yes" rot="MR0">
<attribute name="VALUE" x="149.225" y="10.795" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="SUPPLY4" gate="+5V" x="63.5" y="170.18" smashed="yes">
<attribute name="VALUE" x="61.595" y="173.355" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY6" gate="+5V" x="2.54" y="-10.16" smashed="yes">
<attribute name="VALUE" x="0.635" y="-6.985" size="1.778" layer="96"/>
</instance>
<instance part="X2" gate="-1" x="17.78" y="-25.4" smashed="yes" rot="R270">
<attribute name="NAME" x="17.018" y="-26.416" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="19.685" y="-22.86" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="X2" gate="-2" x="17.78" y="-5.08" smashed="yes" rot="R90">
<attribute name="NAME" x="18.542" y="-4.064" size="1.524" layer="95" rot="R90"/>
</instance>
<instance part="X2" gate="-3" x="22.86" y="-25.4" smashed="yes" rot="MR270">
<attribute name="NAME" x="23.622" y="-26.416" size="1.524" layer="95" rot="MR270"/>
</instance>
<instance part="X2" gate="-4" x="22.86" y="-5.08" smashed="yes" rot="R90">
<attribute name="NAME" x="23.622" y="-4.064" size="1.524" layer="95" rot="R90"/>
</instance>
<instance part="X2" gate="-5" x="27.94" y="-25.4" smashed="yes" rot="R270">
<attribute name="NAME" x="27.178" y="-26.416" size="1.524" layer="95" rot="R270"/>
</instance>
<instance part="X2" gate="-6" x="27.94" y="-5.08" smashed="yes" rot="R90">
<attribute name="NAME" x="28.702" y="-4.064" size="1.524" layer="95" rot="R90"/>
</instance>
<instance part="X3" gate="-1" x="-83.82" y="170.18" smashed="yes">
<attribute name="NAME" x="-82.804" y="169.418" size="1.524" layer="95"/>
<attribute name="VALUE" x="-86.36" y="172.085" size="1.778" layer="96"/>
</instance>
<instance part="X3" gate="-2" x="-60.96" y="170.18" smashed="yes" rot="MR0">
<attribute name="NAME" x="-61.976" y="169.418" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X3" gate="-3" x="-83.82" y="165.1" smashed="yes">
<attribute name="NAME" x="-82.804" y="164.338" size="1.524" layer="95"/>
</instance>
<instance part="X3" gate="-4" x="-60.96" y="165.1" smashed="yes" rot="MR0">
<attribute name="NAME" x="-61.976" y="164.338" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X3" gate="-5" x="-83.82" y="160.02" smashed="yes" rot="MR180">
<attribute name="NAME" x="-82.804" y="160.782" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X3" gate="-6" x="-60.96" y="160.02" smashed="yes" rot="MR0">
<attribute name="NAME" x="-61.976" y="159.258" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X3" gate="-7" x="-83.82" y="154.94" smashed="yes" rot="MR180">
<attribute name="NAME" x="-82.804" y="155.702" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X3" gate="-8" x="-60.96" y="154.94" smashed="yes" rot="MR0">
<attribute name="NAME" x="-61.976" y="154.178" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="X3" gate="-9" x="-83.82" y="149.86" smashed="yes" rot="MR180">
<attribute name="NAME" x="-82.804" y="150.622" size="1.524" layer="95" rot="MR180"/>
</instance>
<instance part="X3" gate="-10" x="-60.96" y="149.86" smashed="yes" rot="MR0">
<attribute name="NAME" x="-61.976" y="149.098" size="1.524" layer="95" rot="MR0"/>
</instance>
<instance part="SUPPLY14" gate="GND" x="-73.66" y="139.7" smashed="yes">
<attribute name="VALUE" x="-75.565" y="136.525" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY15" gate="+5V" x="-35.56" y="162.56" smashed="yes">
<attribute name="VALUE" x="-37.465" y="165.735" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY16" gate="+5V" x="-119.38" y="157.48" smashed="yes">
<attribute name="VALUE" x="-121.285" y="160.655" size="1.778" layer="96"/>
</instance>
<instance part="J2" gate="G$1" x="-66.04" y="101.6" smashed="yes" rot="R270">
<attribute name="NAME" x="-66.04" y="88.9" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-69.85" y="88.9" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J3" gate="G$1" x="-55.88" y="99.06" smashed="yes" rot="R90">
<attribute name="NAME" x="-55.88" y="111.76" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-52.07" y="111.76" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J4" gate="G$2" x="-55.88" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="-55.88" y="50.8" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-52.07" y="50.8" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J5" gate="G$2" x="-66.04" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="-66.04" y="25.4" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-69.85" y="25.4" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J6" gate="G$1" x="76.2" y="99.06" smashed="yes" rot="R270">
<attribute name="NAME" x="76.2" y="86.36" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="72.39" y="86.36" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J7" gate="G$1" x="86.36" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="86.36" y="109.22" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="90.17" y="109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J8" gate="G$2" x="86.36" y="35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="86.36" y="48.26" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="90.17" y="48.26" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J9" gate="G$2" x="76.2" y="35.56" smashed="yes" rot="R270">
<attribute name="NAME" x="76.2" y="22.86" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="72.39" y="22.86" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="X4" gate="-1" x="-50.8" y="-12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="-53.34" y="-11.938" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="-50.038" y="-14.097" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="X4" gate="-2" x="-71.12" y="-12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="-73.66" y="-11.938" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="-70.358" y="-14.097" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<pinref part="X6" gate="-2" pin="S"/>
<wire x1="2.54" y1="-17.78" x2="17.78" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="X2" gate="-1" pin="S"/>
<wire x1="17.78" y1="-17.78" x2="22.86" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-17.78" x2="27.94" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-17.78" x2="43.18" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-17.78" x2="17.78" y2="-20.32" width="0.1524" layer="91"/>
<junction x="17.78" y="-17.78"/>
<pinref part="X2" gate="-3" pin="S"/>
<wire x1="22.86" y1="-20.32" x2="22.86" y2="-17.78" width="0.1524" layer="91"/>
<junction x="22.86" y="-17.78"/>
<pinref part="X2" gate="-5" pin="S"/>
<wire x1="27.94" y1="-20.32" x2="27.94" y2="-17.78" width="0.1524" layer="91"/>
<junction x="27.94" y="-17.78"/>
</segment>
<segment>
<pinref part="X5" gate="-30" pin="S"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="-91.44" y1="99.06" x2="-106.68" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="104.14" x2="-83.82" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="104.14" x2="-83.82" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="99.06" x2="-91.44" y2="99.06" width="0.1524" layer="91"/>
<junction x="-91.44" y="99.06"/>
</segment>
<segment>
<pinref part="X11" gate="-1" pin="S"/>
<wire x1="139.7" y1="167.64" x2="132.08" y2="167.64" width="0.1524" layer="91"/>
<wire x1="132.08" y1="167.64" x2="132.08" y2="177.8" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="132.08" y1="177.8" x2="154.94" y2="177.8" width="0.1524" layer="91"/>
<wire x1="154.94" y1="177.8" x2="154.94" y2="180.34" width="0.1524" layer="91"/>
<wire x1="154.94" y1="177.8" x2="172.72" y2="177.8" width="0.1524" layer="91"/>
<wire x1="172.72" y1="177.8" x2="172.72" y2="167.64" width="0.1524" layer="91"/>
<junction x="154.94" y="177.8"/>
<pinref part="X11" gate="-2" pin="S"/>
<wire x1="172.72" y1="167.64" x2="167.64" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="195.58" y1="-12.7" x2="167.64" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-12.7" x2="167.64" y2="0" width="0.1524" layer="91"/>
<pinref part="X7" gate="-8" pin="S"/>
<wire x1="167.64" y1="0" x2="180.34" y2="0" width="0.1524" layer="91"/>
<pinref part="X7" gate="-7" pin="S"/>
<wire x1="208.28" y1="0" x2="223.52" y2="0" width="0.1524" layer="91"/>
<wire x1="223.52" y1="0" x2="223.52" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="223.52" y1="-12.7" x2="195.58" y2="-12.7" width="0.1524" layer="91"/>
<junction x="195.58" y="-12.7"/>
</segment>
<segment>
<pinref part="X15" gate="-5" pin="S"/>
<wire x1="10.16" y1="154.94" x2="10.16" y2="160.02" width="0.1524" layer="91"/>
<wire x1="10.16" y1="160.02" x2="15.24" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<wire x1="22.86" y1="154.94" x2="10.16" y2="154.94" width="0.1524" layer="91"/>
<wire x1="22.86" y1="147.32" x2="22.86" y2="154.94" width="0.1524" layer="91"/>
<junction x="22.86" y="154.94"/>
<wire x1="22.86" y1="154.94" x2="53.34" y2="154.94" width="0.1524" layer="91"/>
<wire x1="53.34" y1="154.94" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<pinref part="X15" gate="-6" pin="S"/>
<wire x1="53.34" y1="160.02" x2="48.26" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="-20" pin="S"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="111.76" y1="60.96" x2="129.54" y2="60.96" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$2" pin="8"/>
<wire x1="88.9" y1="43.18" x2="91.44" y2="43.18" width="0.1524" layer="91"/>
<wire x1="91.44" y1="43.18" x2="91.44" y2="60.96" width="0.1524" layer="91"/>
<wire x1="91.44" y1="60.96" x2="111.76" y2="60.96" width="0.1524" layer="91"/>
<junction x="111.76" y="60.96"/>
</segment>
<segment>
<pinref part="X3" gate="-9" pin="S"/>
<wire x1="-88.9" y1="149.86" x2="-96.52" y2="149.86" width="0.1524" layer="91"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="-73.66" y1="142.24" x2="-96.52" y2="142.24" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="142.24" x2="-96.52" y2="149.86" width="0.1524" layer="91"/>
<pinref part="X3" gate="-10" pin="S"/>
<wire x1="-55.88" y1="149.86" x2="-48.26" y2="149.86" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="142.24" x2="-48.26" y2="142.24" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="142.24" x2="-48.26" y2="149.86" width="0.1524" layer="91"/>
<junction x="-73.66" y="142.24"/>
</segment>
<segment>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="251.46" y1="53.34" x2="251.46" y2="58.42" width="0.1524" layer="91"/>
<pinref part="X13" gate="-12" pin="S"/>
<wire x1="233.68" y1="58.42" x2="251.46" y2="58.42" width="0.1524" layer="91"/>
<wire x1="251.46" y1="68.58" x2="251.46" y2="58.42" width="0.1524" layer="91"/>
<junction x="251.46" y="58.42"/>
<pinref part="X13" gate="-6" pin="S"/>
<wire x1="251.46" y1="68.58" x2="238.76" y2="68.58" width="0.1524" layer="91"/>
<wire x1="238.76" y1="68.58" x2="233.68" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="-2.54" y1="167.64" x2="-2.54" y2="165.1" width="0.1524" layer="91"/>
<pinref part="X15" gate="-3" pin="S"/>
<wire x1="-2.54" y1="165.1" x2="15.24" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SUPPLY12" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="X11" gate="-3" pin="S"/>
<pinref part="SUPPLY9" gate="+5V" pin="+5V"/>
<wire x1="121.92" y1="165.1" x2="121.92" y2="162.56" width="0.1524" layer="91"/>
<wire x1="121.92" y1="162.56" x2="139.7" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X11" gate="-4" pin="S"/>
<pinref part="SUPPLY2" gate="+5V" pin="+5V"/>
<wire x1="167.64" y1="162.56" x2="185.42" y2="162.56" width="0.1524" layer="91"/>
<wire x1="185.42" y1="162.56" x2="185.42" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X7" gate="-6" pin="S"/>
<pinref part="SUPPLY3" gate="+5V" pin="+5V"/>
<wire x1="180.34" y1="5.08" x2="147.32" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X6" gate="-1" pin="S"/>
<pinref part="SUPPLY6" gate="+5V" pin="+5V"/>
<wire x1="43.18" y1="-12.7" x2="27.94" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="X2" gate="-4" pin="S"/>
<wire x1="27.94" y1="-12.7" x2="22.86" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-12.7" x2="17.78" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-12.7" x2="2.54" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-10.16" x2="22.86" y2="-12.7" width="0.1524" layer="91"/>
<junction x="22.86" y="-12.7"/>
<pinref part="X2" gate="-6" pin="S"/>
<wire x1="27.94" y1="-10.16" x2="27.94" y2="-12.7" width="0.1524" layer="91"/>
<junction x="27.94" y="-12.7"/>
<pinref part="X2" gate="-2" pin="S"/>
<wire x1="17.78" y1="-10.16" x2="17.78" y2="-12.7" width="0.1524" layer="91"/>
<junction x="17.78" y="-12.7"/>
</segment>
<segment>
<pinref part="X15" gate="-4" pin="S"/>
<pinref part="SUPPLY4" gate="+5V" pin="+5V"/>
<wire x1="48.26" y1="165.1" x2="63.5" y2="165.1" width="0.1524" layer="91"/>
<wire x1="63.5" y1="165.1" x2="63.5" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X7" gate="-5" pin="S"/>
<pinref part="SUPPLY10" gate="+5V" pin="+5V"/>
<wire x1="208.28" y1="5.08" x2="238.76" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="-7" pin="S"/>
<pinref part="SUPPLY16" gate="+5V" pin="+5V"/>
<wire x1="-88.9" y1="154.94" x2="-119.38" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="-6" pin="S"/>
<pinref part="SUPPLY15" gate="+5V" pin="+5V"/>
<wire x1="-35.56" y1="160.02" x2="-55.88" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY17" gate="+5V" pin="+5V"/>
<pinref part="X13" gate="-4" pin="S"/>
<wire x1="251.46" y1="83.82" x2="238.76" y2="83.82" width="0.1524" layer="91"/>
<wire x1="238.76" y1="83.82" x2="233.68" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ROUE_MOTEUR_2_ENCODEUR_A" class="0">
<segment>
<pinref part="X1" gate="-18" pin="S"/>
<wire x1="111.76" y1="66.04" x2="116.84" y2="66.04" width="0.1524" layer="91"/>
<label x="116.84" y="66.04" size="1.778" layer="95"/>
<pinref part="J8" gate="G$2" pin="9"/>
<wire x1="88.9" y1="45.72" x2="88.9" y2="66.04" width="0.1524" layer="91"/>
<wire x1="88.9" y1="66.04" x2="111.76" y2="66.04" width="0.1524" layer="91"/>
<junction x="111.76" y="66.04"/>
</segment>
<segment>
<pinref part="X13" gate="-5" pin="S"/>
<wire x1="203.2" y1="73.66" x2="198.12" y2="73.66" width="0.1524" layer="91"/>
<label x="160.02" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="ROUE_MOTEUR_1_PWM_0" class="0">
<segment>
<pinref part="X5" gate="-27" pin="S"/>
<wire x1="-27.94" y1="93.98" x2="-22.86" y2="93.98" width="0.1524" layer="91"/>
<label x="-22.86" y="93.98" size="1.778" layer="95"/>
<pinref part="J3" gate="G$1" pin="5"/>
<wire x1="-53.34" y1="101.6" x2="-38.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="101.6" x2="-38.1" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="93.98" x2="-27.94" y2="93.98" width="0.1524" layer="91"/>
<junction x="-27.94" y="93.98"/>
</segment>
<segment>
<pinref part="X13" gate="-9" pin="S"/>
<wire x1="203.2" y1="63.5" x2="198.12" y2="63.5" width="0.1524" layer="91"/>
<label x="160.02" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="ROUE_MOTEUR_2_ENCODEUR_B" class="0">
<segment>
<pinref part="X1" gate="-22" pin="S"/>
<wire x1="111.76" y1="55.88" x2="116.84" y2="55.88" width="0.1524" layer="91"/>
<label x="116.84" y="55.88" size="1.778" layer="95"/>
<pinref part="J8" gate="G$2" pin="7"/>
<wire x1="88.9" y1="40.64" x2="93.98" y2="40.64" width="0.1524" layer="91"/>
<wire x1="93.98" y1="40.64" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
<wire x1="93.98" y1="55.88" x2="111.76" y2="55.88" width="0.1524" layer="91"/>
<junction x="111.76" y="55.88"/>
</segment>
<segment>
<pinref part="X13" gate="-7" pin="S"/>
<wire x1="203.2" y1="68.58" x2="198.12" y2="68.58" width="0.1524" layer="91"/>
<label x="160.02" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="ROUE_MOTEUR_1_ENCODEUR_A" class="0">
<segment>
<pinref part="X5" gate="-18" pin="S"/>
<wire x1="-91.44" y1="68.58" x2="-96.52" y2="68.58" width="0.1524" layer="91"/>
<label x="-132.08" y="68.58" size="1.778" layer="95"/>
<pinref part="J5" gate="G$2" pin="1"/>
<wire x1="-68.58" y1="48.26" x2="-71.12" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="48.26" x2="-71.12" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="68.58" x2="-91.44" y2="68.58" width="0.1524" layer="91"/>
<junction x="-91.44" y="68.58"/>
</segment>
<segment>
<pinref part="X13" gate="-1" pin="S"/>
<wire x1="203.2" y1="83.82" x2="198.12" y2="83.82" width="0.1524" layer="91"/>
<label x="160.02" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="ROUE_MOTEUR_1_ENCODEUR_B" class="0">
<segment>
<pinref part="X5" gate="-22" pin="S"/>
<wire x1="-91.44" y1="78.74" x2="-96.52" y2="78.74" width="0.1524" layer="91"/>
<label x="-132.08" y="78.74" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="7"/>
<wire x1="-68.58" y1="93.98" x2="-73.66" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="93.98" x2="-73.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="78.74" x2="-91.44" y2="78.74" width="0.1524" layer="91"/>
<junction x="-91.44" y="78.74"/>
</segment>
<segment>
<pinref part="X13" gate="-3" pin="S"/>
<wire x1="203.2" y1="78.74" x2="198.12" y2="78.74" width="0.1524" layer="91"/>
<label x="160.02" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="ROUE_MOTEUR_2_PWM_1" class="0">
<segment>
<pinref part="X5" gate="-29" pin="S"/>
<wire x1="-27.94" y1="99.06" x2="-22.86" y2="99.06" width="0.1524" layer="91"/>
<label x="-22.86" y="99.06" size="1.778" layer="95"/>
<pinref part="J3" gate="G$1" pin="6"/>
<wire x1="-53.34" y1="104.14" x2="-35.56" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="104.14" x2="-35.56" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="99.06" x2="-27.94" y2="99.06" width="0.1524" layer="91"/>
<junction x="-27.94" y="99.06"/>
</segment>
<segment>
<pinref part="X13" gate="-11" pin="S"/>
<wire x1="203.2" y1="58.42" x2="198.12" y2="58.42" width="0.1524" layer="91"/>
<label x="160.02" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="TRIEUR_PWM_0" class="0">
<segment>
<pinref part="X1" gate="-27" pin="S"/>
<wire x1="48.26" y1="40.64" x2="43.18" y2="40.64" width="0.1524" layer="91"/>
<label x="25.4" y="40.64" size="1.778" layer="95"/>
<pinref part="J9" gate="G$2" pin="6"/>
<wire x1="73.66" y1="33.02" x2="58.42" y2="33.02" width="0.1524" layer="91"/>
<wire x1="58.42" y1="33.02" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<wire x1="58.42" y1="40.64" x2="48.26" y2="40.64" width="0.1524" layer="91"/>
<junction x="48.26" y="40.64"/>
</segment>
<segment>
<pinref part="X11" gate="-6" pin="S"/>
<wire x1="167.64" y1="157.48" x2="172.72" y2="157.48" width="0.1524" layer="91"/>
<label x="172.72" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="TRIEUR_PWM_2" class="0">
<segment>
<pinref part="X1" gate="-29" pin="S"/>
<wire x1="48.26" y1="35.56" x2="43.18" y2="35.56" width="0.1524" layer="91"/>
<label x="25.4" y="35.56" size="1.778" layer="95"/>
<pinref part="J9" gate="G$2" pin="7"/>
<wire x1="73.66" y1="30.48" x2="55.88" y2="30.48" width="0.1524" layer="91"/>
<wire x1="55.88" y1="30.48" x2="55.88" y2="35.56" width="0.1524" layer="91"/>
<wire x1="55.88" y1="35.56" x2="48.26" y2="35.56" width="0.1524" layer="91"/>
<junction x="48.26" y="35.56"/>
</segment>
<segment>
<pinref part="X11" gate="-5" pin="S"/>
<wire x1="139.7" y1="157.48" x2="137.16" y2="157.48" width="0.1524" layer="91"/>
<label x="116.84" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="SUIVEUR_I2C_SCL" class="0">
<segment>
<pinref part="X5" gate="-32" pin="S"/>
<wire x1="-91.44" y1="104.14" x2="-96.52" y2="104.14" width="0.1524" layer="91"/>
<label x="-116.84" y="104.14" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="-68.58" y1="106.68" x2="-86.36" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="106.68" x2="-86.36" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="104.14" x2="-91.44" y2="104.14" width="0.1524" layer="91"/>
<junction x="-91.44" y="104.14"/>
</segment>
<segment>
<pinref part="X7" gate="-4" pin="S"/>
<wire x1="180.34" y1="10.16" x2="175.26" y2="10.16" width="0.1524" layer="91"/>
<label x="154.94" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="SUIVEUR_IDC_SDA" class="0">
<segment>
<pinref part="X5" gate="-34" pin="S"/>
<wire x1="-91.44" y1="109.22" x2="-96.52" y2="109.22" width="0.1524" layer="91"/>
<label x="-116.84" y="109.22" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="-68.58" y1="109.22" x2="-91.44" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X7" gate="-2" pin="S"/>
<wire x1="180.34" y1="15.24" x2="175.26" y2="15.24" width="0.1524" layer="91"/>
<label x="154.94" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="LANC_PWM_2" class="0">
<segment>
<pinref part="X5" gate="-31" pin="S"/>
<wire x1="-27.94" y1="104.14" x2="-22.86" y2="104.14" width="0.1524" layer="91"/>
<label x="-22.86" y="104.14" size="1.778" layer="95"/>
<pinref part="J3" gate="G$1" pin="7"/>
<wire x1="-53.34" y1="106.68" x2="-33.02" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="106.68" x2="-33.02" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="104.14" x2="-27.94" y2="104.14" width="0.1524" layer="91"/>
<junction x="-27.94" y="104.14"/>
</segment>
<segment>
<pinref part="X15" gate="-1" pin="S"/>
<wire x1="15.24" y1="170.18" x2="7.62" y2="170.18" width="0.1524" layer="91"/>
<label x="2.54" y="175.26" size="1.778" layer="95"/>
<wire x1="7.62" y1="170.18" x2="7.62" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ULTRA_D1" class="0">
<segment>
<pinref part="X1" gate="-11" pin="S"/>
<wire x1="48.26" y1="81.28" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<label x="30.48" y="81.28" size="1.778" layer="95"/>
<pinref part="J6" gate="G$1" pin="6"/>
<wire x1="73.66" y1="93.98" x2="63.5" y2="93.98" width="0.1524" layer="91"/>
<wire x1="63.5" y1="93.98" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
<wire x1="63.5" y1="81.28" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
<junction x="48.26" y="81.28"/>
</segment>
</net>
<net name="ULTRA_D2" class="0">
<segment>
<pinref part="X1" gate="-13" pin="S"/>
<wire x1="48.26" y1="76.2" x2="43.18" y2="76.2" width="0.1524" layer="91"/>
<label x="30.48" y="76.2" size="1.778" layer="95"/>
<wire x1="48.26" y1="76.2" x2="66.04" y2="76.2" width="0.1524" layer="91"/>
<wire x1="66.04" y1="76.2" x2="66.04" y2="91.44" width="0.1524" layer="91"/>
<junction x="48.26" y="76.2"/>
<pinref part="J6" gate="G$1" pin="7"/>
<wire x1="66.04" y1="91.44" x2="73.66" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BRAS_PWM" class="0">
<segment>
<pinref part="X1" gate="-31" pin="S"/>
<wire x1="48.26" y1="30.48" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
<label x="30.48" y="30.48" size="1.778" layer="95"/>
<pinref part="J9" gate="G$2" pin="8"/>
<wire x1="73.66" y1="27.94" x2="53.34" y2="27.94" width="0.1524" layer="91"/>
<wire x1="53.34" y1="27.94" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<wire x1="53.34" y1="30.48" x2="48.26" y2="30.48" width="0.1524" layer="91"/>
<junction x="48.26" y="30.48"/>
</segment>
<segment>
<pinref part="X7" gate="-3" pin="S"/>
<wire x1="208.28" y1="10.16" x2="210.82" y2="10.16" width="0.1524" layer="91"/>
<label x="210.82" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="4B" class="0">
<segment>
<pinref part="X1" gate="-4" pin="S"/>
<wire x1="111.76" y1="101.6" x2="109.22" y2="101.6" width="0.1524" layer="91"/>
<wire x1="111.76" y1="101.6" x2="106.68" y2="101.6" width="0.1524" layer="91"/>
<wire x1="106.68" y1="101.6" x2="106.68" y2="104.14" width="0.1524" layer="91"/>
<junction x="111.76" y="101.6"/>
<pinref part="J7" gate="G$1" pin="7"/>
<wire x1="106.68" y1="104.14" x2="88.9" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="106.68" y1="101.6" x2="116.84" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<pinref part="X1" gate="-6" pin="S"/>
<wire x1="111.76" y1="96.52" x2="116.84" y2="96.52" width="0.1524" layer="91"/>
<label x="116.84" y="96.52" size="1.778" layer="95"/>
<wire x1="111.76" y1="96.52" x2="104.14" y2="96.52" width="0.1524" layer="91"/>
<wire x1="104.14" y1="96.52" x2="104.14" y2="101.6" width="0.1524" layer="91"/>
<junction x="111.76" y="96.52"/>
<pinref part="J7" gate="G$1" pin="6"/>
<wire x1="104.14" y1="101.6" x2="88.9" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="223.52" y1="121.92" x2="228.6" y2="121.92" width="0.1524" layer="91"/>
<wire x1="228.6" y1="121.92" x2="228.6" y2="127" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="228.6" y1="127" x2="228.6" y2="132.08" width="0.1524" layer="91"/>
<wire x1="228.6" y1="132.08" x2="223.52" y2="132.08" width="0.1524" layer="91"/>
<label x="233.68" y="127" size="1.778" layer="95"/>
<junction x="228.6" y="127"/>
<wire x1="228.6" y1="127" x2="238.76" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A_MODIF_POUR_DATA_MOTEUR_1.5V" class="0">
<segment>
<label x="200.66" y="180.34" size="1.778" layer="95"/>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="X12" gate="-5" pin="S"/>
<wire x1="203.2" y1="157.48" x2="198.12" y2="157.48" width="0.1524" layer="91"/>
<wire x1="198.12" y1="157.48" x2="198.12" y2="132.08" width="0.1524" layer="91"/>
<wire x1="198.12" y1="132.08" x2="213.36" y2="132.08" width="0.1524" layer="91"/>
<label x="236.22" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="X5" gate="-28" pin="S"/>
<wire x1="-91.44" y1="93.98" x2="-93.98" y2="93.98" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="4"/>
<wire x1="-68.58" y1="101.6" x2="-81.28" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="101.6" x2="-81.28" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="93.98" x2="-91.44" y2="93.98" width="0.1524" layer="91"/>
<junction x="-91.44" y="93.98"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="X5" gate="-26" pin="S"/>
<wire x1="-91.44" y1="88.9" x2="-93.98" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="5"/>
<wire x1="-68.58" y1="99.06" x2="-78.74" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="99.06" x2="-78.74" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="88.9" x2="-91.44" y2="88.9" width="0.1524" layer="91"/>
<junction x="-91.44" y="88.9"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="X5" gate="-24" pin="S"/>
<wire x1="-91.44" y1="83.82" x2="-96.52" y2="83.82" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="6"/>
<wire x1="-68.58" y1="96.52" x2="-76.2" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="96.52" x2="-76.2" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="83.82" x2="-91.44" y2="83.82" width="0.1524" layer="91"/>
<junction x="-91.44" y="83.82"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="X5" gate="-16" pin="S"/>
<wire x1="-91.44" y1="63.5" x2="-96.52" y2="63.5" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$2" pin="2"/>
<wire x1="-68.58" y1="45.72" x2="-73.66" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="45.72" x2="-73.66" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="63.5" x2="-91.44" y2="63.5" width="0.1524" layer="91"/>
<junction x="-91.44" y="63.5"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="X5" gate="-12" pin="S"/>
<wire x1="-91.44" y1="53.34" x2="-96.52" y2="53.34" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$2" pin="4"/>
<wire x1="-68.58" y1="40.64" x2="-78.74" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="40.64" x2="-78.74" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="53.34" x2="-91.44" y2="53.34" width="0.1524" layer="91"/>
<junction x="-91.44" y="53.34"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="X5" gate="-8" pin="S"/>
<wire x1="-91.44" y1="43.18" x2="-96.52" y2="43.18" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$2" pin="6"/>
<wire x1="-68.58" y1="35.56" x2="-83.82" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="35.56" x2="-83.82" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="43.18" x2="-91.44" y2="43.18" width="0.1524" layer="91"/>
<junction x="-91.44" y="43.18"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="X5" gate="-4" pin="S"/>
<wire x1="-91.44" y1="33.02" x2="-96.52" y2="33.02" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$2" pin="8"/>
<wire x1="-68.58" y1="30.48" x2="-88.9" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="30.48" x2="-88.9" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="33.02" x2="-91.44" y2="33.02" width="0.1524" layer="91"/>
<junction x="-91.44" y="33.02"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="X5" gate="-2" pin="S"/>
<wire x1="-91.44" y1="27.94" x2="-93.98" y2="27.94" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$2" pin="9"/>
<wire x1="-93.98" y1="27.94" x2="-96.52" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="27.94" x2="-91.44" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="X5" gate="-33" pin="S"/>
<wire x1="-27.94" y1="109.22" x2="-22.86" y2="109.22" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="8"/>
<wire x1="-53.34" y1="109.22" x2="-27.94" y2="109.22" width="0.1524" layer="91"/>
<junction x="-27.94" y="109.22"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="X5" gate="-25" pin="S"/>
<wire x1="-27.94" y1="88.9" x2="-22.86" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="88.9" x2="-40.64" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="88.9" x2="-40.64" y2="99.06" width="0.1524" layer="91"/>
<junction x="-27.94" y="88.9"/>
<pinref part="J3" gate="G$1" pin="4"/>
<wire x1="-40.64" y1="99.06" x2="-53.34" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="X5" gate="-23" pin="S"/>
<wire x1="-27.94" y1="83.82" x2="-22.86" y2="83.82" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="3"/>
<wire x1="-53.34" y1="96.52" x2="-43.18" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="96.52" x2="-43.18" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="83.82" x2="-27.94" y2="83.82" width="0.1524" layer="91"/>
<junction x="-27.94" y="83.82"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="X5" gate="-21" pin="S"/>
<wire x1="-27.94" y1="78.74" x2="-22.86" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="78.74" x2="-45.72" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="78.74" x2="-45.72" y2="93.98" width="0.1524" layer="91"/>
<junction x="-27.94" y="78.74"/>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="93.98" x2="-53.34" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="X5" gate="-19" pin="S"/>
<wire x1="-27.94" y1="73.66" x2="-22.86" y2="73.66" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="-53.34" y1="91.44" x2="-48.26" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="91.44" x2="-48.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="73.66" x2="-27.94" y2="73.66" width="0.1524" layer="91"/>
<junction x="-27.94" y="73.66"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="X5" gate="-17" pin="S"/>
<wire x1="-27.94" y1="68.58" x2="-22.86" y2="68.58" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$2" pin="9"/>
<wire x1="-53.34" y1="48.26" x2="-48.26" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="48.26" x2="-48.26" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="68.58" x2="-27.94" y2="68.58" width="0.1524" layer="91"/>
<junction x="-27.94" y="68.58"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="X5" gate="-15" pin="S"/>
<wire x1="-27.94" y1="63.5" x2="-22.86" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="63.5" x2="-45.72" y2="63.5" width="0.1524" layer="91"/>
<junction x="-27.94" y="63.5"/>
<wire x1="-45.72" y1="63.5" x2="-45.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$2" pin="8"/>
<wire x1="-45.72" y1="45.72" x2="-53.34" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="X5" gate="-13" pin="S"/>
<wire x1="-27.94" y1="58.42" x2="-22.86" y2="58.42" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$2" pin="7"/>
<wire x1="-53.34" y1="43.18" x2="-43.18" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="43.18" x2="-43.18" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="58.42" x2="-27.94" y2="58.42" width="0.1524" layer="91"/>
<junction x="-27.94" y="58.42"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="X5" gate="-11" pin="S"/>
<wire x1="-27.94" y1="53.34" x2="-22.86" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="53.34" x2="-40.64" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="53.34" x2="-40.64" y2="40.64" width="0.1524" layer="91"/>
<junction x="-27.94" y="53.34"/>
<wire x1="-40.64" y1="40.64" x2="-53.34" y2="40.64" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$2" pin="6"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="X5" gate="-9" pin="S"/>
<wire x1="-27.94" y1="48.26" x2="-22.86" y2="48.26" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$2" pin="5"/>
<wire x1="-53.34" y1="38.1" x2="-38.1" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="38.1" x2="-38.1" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="48.26" x2="-27.94" y2="48.26" width="0.1524" layer="91"/>
<junction x="-27.94" y="48.26"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="X5" gate="-7" pin="S"/>
<wire x1="-27.94" y1="43.18" x2="-22.86" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="43.18" x2="-35.56" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="43.18" x2="-35.56" y2="35.56" width="0.1524" layer="91"/>
<junction x="-27.94" y="43.18"/>
<pinref part="J4" gate="G$2" pin="4"/>
<wire x1="-35.56" y1="35.56" x2="-53.34" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="X5" gate="-5" pin="S"/>
<wire x1="-27.94" y1="38.1" x2="-22.86" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="38.1" x2="-33.02" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="38.1" x2="-33.02" y2="33.02" width="0.1524" layer="91"/>
<junction x="-27.94" y="38.1"/>
<pinref part="J4" gate="G$2" pin="3"/>
<wire x1="-33.02" y1="33.02" x2="-53.34" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="X5" gate="-3" pin="S"/>
<wire x1="-27.94" y1="33.02" x2="-22.86" y2="33.02" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$2" pin="2"/>
<wire x1="-53.34" y1="30.48" x2="-30.48" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="30.48" x2="-30.48" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="33.02" x2="-27.94" y2="33.02" width="0.1524" layer="91"/>
<junction x="-27.94" y="33.02"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="X5" gate="-1" pin="S"/>
<wire x1="-27.94" y1="27.94" x2="-22.86" y2="27.94" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$2" pin="1"/>
<wire x1="-53.34" y1="27.94" x2="-27.94" y2="27.94" width="0.1524" layer="91"/>
<junction x="-27.94" y="27.94"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="X1" gate="-2" pin="S"/>
<wire x1="111.76" y1="106.68" x2="116.84" y2="106.68" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="8"/>
<wire x1="88.9" y1="106.68" x2="111.76" y2="106.68" width="0.1524" layer="91"/>
<junction x="111.76" y="106.68"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="X1" gate="-8" pin="S"/>
<wire x1="111.76" y1="91.44" x2="116.84" y2="91.44" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="5"/>
<wire x1="88.9" y1="99.06" x2="101.6" y2="99.06" width="0.1524" layer="91"/>
<wire x1="101.6" y1="99.06" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
<wire x1="101.6" y1="91.44" x2="111.76" y2="91.44" width="0.1524" layer="91"/>
<junction x="111.76" y="91.44"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="X1" gate="-12" pin="S"/>
<wire x1="111.76" y1="81.28" x2="116.84" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="3"/>
<wire x1="88.9" y1="93.98" x2="96.52" y2="93.98" width="0.1524" layer="91"/>
<wire x1="96.52" y1="93.98" x2="96.52" y2="81.28" width="0.1524" layer="91"/>
<wire x1="96.52" y1="81.28" x2="111.76" y2="81.28" width="0.1524" layer="91"/>
<junction x="111.76" y="81.28"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="X1" gate="-16" pin="S"/>
<wire x1="111.76" y1="71.12" x2="116.84" y2="71.12" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="88.9" y1="88.9" x2="91.44" y2="88.9" width="0.1524" layer="91"/>
<wire x1="91.44" y1="88.9" x2="91.44" y2="71.12" width="0.1524" layer="91"/>
<wire x1="91.44" y1="71.12" x2="111.76" y2="71.12" width="0.1524" layer="91"/>
<junction x="111.76" y="71.12"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="X1" gate="-24" pin="S"/>
<wire x1="111.76" y1="50.8" x2="116.84" y2="50.8" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$2" pin="6"/>
<wire x1="88.9" y1="38.1" x2="96.52" y2="38.1" width="0.1524" layer="91"/>
<wire x1="96.52" y1="38.1" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="50.8" x2="111.76" y2="50.8" width="0.1524" layer="91"/>
<junction x="111.76" y="50.8"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="X1" gate="-28" pin="S"/>
<wire x1="111.76" y1="40.64" x2="116.84" y2="40.64" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$2" pin="4"/>
<wire x1="88.9" y1="33.02" x2="101.6" y2="33.02" width="0.1524" layer="91"/>
<wire x1="101.6" y1="33.02" x2="101.6" y2="40.64" width="0.1524" layer="91"/>
<wire x1="101.6" y1="40.64" x2="111.76" y2="40.64" width="0.1524" layer="91"/>
<junction x="111.76" y="40.64"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="X1" gate="-30" pin="S"/>
<wire x1="111.76" y1="35.56" x2="116.84" y2="35.56" width="0.1524" layer="91"/>
<wire x1="111.76" y1="35.56" x2="104.14" y2="35.56" width="0.1524" layer="91"/>
<wire x1="104.14" y1="35.56" x2="104.14" y2="30.48" width="0.1524" layer="91"/>
<junction x="111.76" y="35.56"/>
<pinref part="J8" gate="G$2" pin="3"/>
<wire x1="104.14" y1="30.48" x2="88.9" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="X1" gate="-32" pin="S"/>
<wire x1="111.76" y1="30.48" x2="116.84" y2="30.48" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$2" pin="2"/>
<wire x1="88.9" y1="27.94" x2="106.68" y2="27.94" width="0.1524" layer="91"/>
<wire x1="106.68" y1="27.94" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<wire x1="106.68" y1="30.48" x2="111.76" y2="30.48" width="0.1524" layer="91"/>
<junction x="111.76" y="30.48"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="X1" gate="-34" pin="S"/>
<wire x1="111.76" y1="25.4" x2="116.84" y2="25.4" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$2" pin="1"/>
<wire x1="88.9" y1="25.4" x2="111.76" y2="25.4" width="0.1524" layer="91"/>
<junction x="111.76" y="25.4"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="X1" gate="-33" pin="S"/>
<wire x1="48.26" y1="25.4" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$2" pin="9"/>
<wire x1="73.66" y1="25.4" x2="48.26" y2="25.4" width="0.1524" layer="91"/>
<junction x="48.26" y="25.4"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="X1" gate="-25" pin="S"/>
<wire x1="48.26" y1="45.72" x2="43.18" y2="45.72" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$2" pin="5"/>
<wire x1="73.66" y1="35.56" x2="60.96" y2="35.56" width="0.1524" layer="91"/>
<wire x1="60.96" y1="35.56" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<wire x1="60.96" y1="45.72" x2="48.26" y2="45.72" width="0.1524" layer="91"/>
<junction x="48.26" y="45.72"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="X1" gate="-23" pin="S"/>
<wire x1="48.26" y1="50.8" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$2" pin="4"/>
<wire x1="73.66" y1="38.1" x2="63.5" y2="38.1" width="0.1524" layer="91"/>
<wire x1="63.5" y1="38.1" x2="63.5" y2="50.8" width="0.1524" layer="91"/>
<wire x1="63.5" y1="50.8" x2="48.26" y2="50.8" width="0.1524" layer="91"/>
<junction x="48.26" y="50.8"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="X1" gate="-21" pin="S"/>
<wire x1="48.26" y1="55.88" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$2" pin="3"/>
<wire x1="73.66" y1="40.64" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="40.64" x2="66.04" y2="55.88" width="0.1524" layer="91"/>
<wire x1="66.04" y1="55.88" x2="48.26" y2="55.88" width="0.1524" layer="91"/>
<junction x="48.26" y="55.88"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="X1" gate="-19" pin="S"/>
<wire x1="48.26" y1="60.96" x2="43.18" y2="60.96" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$2" pin="2"/>
<wire x1="73.66" y1="43.18" x2="68.58" y2="43.18" width="0.1524" layer="91"/>
<wire x1="68.58" y1="43.18" x2="68.58" y2="60.96" width="0.1524" layer="91"/>
<wire x1="68.58" y1="60.96" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
<junction x="48.26" y="60.96"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="X1" gate="-17" pin="S"/>
<wire x1="48.26" y1="66.04" x2="43.18" y2="66.04" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$2" pin="1"/>
<wire x1="73.66" y1="45.72" x2="71.12" y2="45.72" width="0.1524" layer="91"/>
<wire x1="71.12" y1="45.72" x2="71.12" y2="66.04" width="0.1524" layer="91"/>
<wire x1="71.12" y1="66.04" x2="48.26" y2="66.04" width="0.1524" layer="91"/>
<junction x="48.26" y="66.04"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="X1" gate="-15" pin="S"/>
<wire x1="48.26" y1="71.12" x2="43.18" y2="71.12" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="8"/>
<wire x1="73.66" y1="88.9" x2="68.58" y2="88.9" width="0.1524" layer="91"/>
<wire x1="68.58" y1="88.9" x2="68.58" y2="71.12" width="0.1524" layer="91"/>
<wire x1="68.58" y1="71.12" x2="48.26" y2="71.12" width="0.1524" layer="91"/>
<junction x="48.26" y="71.12"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="X1" gate="-9" pin="S"/>
<wire x1="48.26" y1="86.36" x2="43.18" y2="86.36" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="5"/>
<wire x1="73.66" y1="96.52" x2="60.96" y2="96.52" width="0.1524" layer="91"/>
<wire x1="60.96" y1="96.52" x2="60.96" y2="86.36" width="0.1524" layer="91"/>
<wire x1="60.96" y1="86.36" x2="48.26" y2="86.36" width="0.1524" layer="91"/>
<junction x="48.26" y="86.36"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="X1" gate="-7" pin="S"/>
<wire x1="48.26" y1="91.44" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="4"/>
<wire x1="73.66" y1="99.06" x2="58.42" y2="99.06" width="0.1524" layer="91"/>
<wire x1="58.42" y1="99.06" x2="58.42" y2="91.44" width="0.1524" layer="91"/>
<wire x1="58.42" y1="91.44" x2="48.26" y2="91.44" width="0.1524" layer="91"/>
<junction x="48.26" y="91.44"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="X1" gate="-5" pin="S"/>
<wire x1="48.26" y1="96.52" x2="43.18" y2="96.52" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="3"/>
<wire x1="73.66" y1="101.6" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
<wire x1="55.88" y1="101.6" x2="55.88" y2="96.52" width="0.1524" layer="91"/>
<wire x1="55.88" y1="96.52" x2="48.26" y2="96.52" width="0.1524" layer="91"/>
<junction x="48.26" y="96.52"/>
</segment>
</net>
<net name="SUIVEUR_A" class="0">
<segment>
<pinref part="X1" gate="-3" pin="S"/>
<wire x1="48.26" y1="101.6" x2="43.18" y2="101.6" width="0.1524" layer="91"/>
<label x="30.48" y="101.6" size="1.778" layer="95"/>
<pinref part="J6" gate="G$1" pin="2"/>
<wire x1="73.66" y1="104.14" x2="53.34" y2="104.14" width="0.1524" layer="91"/>
<wire x1="53.34" y1="104.14" x2="53.34" y2="101.6" width="0.1524" layer="91"/>
<wire x1="53.34" y1="101.6" x2="48.26" y2="101.6" width="0.1524" layer="91"/>
<junction x="48.26" y="101.6"/>
</segment>
<segment>
<pinref part="X12" gate="-1" pin="S"/>
<wire x1="203.2" y1="167.64" x2="200.66" y2="167.64" width="0.1524" layer="91"/>
<label x="200.66" y="172.72" size="1.778" layer="95"/>
<wire x1="200.66" y1="167.64" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<wire x1="200.66" y1="170.18" x2="190.5" y2="170.18" width="0.1524" layer="91"/>
<wire x1="190.5" y1="170.18" x2="190.5" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="190.5" y1="121.92" x2="213.36" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ALIM_+5V" class="0">
<segment>
<pinref part="X12" gate="-3" pin="S"/>
<label x="193.04" y="162.56" size="1.778" layer="95"/>
<wire x1="193.04" y1="162.56" x2="203.2" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="-1" pin="S"/>
<wire x1="48.26" y1="106.68" x2="43.18" y2="106.68" width="0.1524" layer="91"/>
<label x="30.48" y="106.68" size="1.778" layer="95"/>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="73.66" y1="106.68" x2="48.26" y2="106.68" width="0.1524" layer="91"/>
<junction x="48.26" y="106.68"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="X12" gate="-4" pin="S"/>
<wire x1="231.14" y1="162.56" x2="236.22" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="X12" gate="-6" pin="S"/>
<wire x1="231.14" y1="157.48" x2="236.22" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LIDAR_RX" class="0">
<segment>
<pinref part="X3" gate="-3" pin="S"/>
<wire x1="-88.9" y1="165.1" x2="-96.52" y2="165.1" width="0.1524" layer="91"/>
<label x="-106.68" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-14" pin="S"/>
<wire x1="111.76" y1="76.2" x2="116.84" y2="76.2" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="88.9" y1="91.44" x2="93.98" y2="91.44" width="0.1524" layer="91"/>
<wire x1="93.98" y1="91.44" x2="93.98" y2="76.2" width="0.1524" layer="91"/>
<wire x1="93.98" y1="76.2" x2="111.76" y2="76.2" width="0.1524" layer="91"/>
<junction x="111.76" y="76.2"/>
<label x="116.84" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="LIDAR_TX" class="0">
<segment>
<pinref part="X3" gate="-5" pin="S"/>
<wire x1="-88.9" y1="160.02" x2="-96.52" y2="160.02" width="0.1524" layer="91"/>
<label x="-106.68" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-10" pin="S"/>
<wire x1="111.76" y1="86.36" x2="116.84" y2="86.36" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="4"/>
<wire x1="88.9" y1="96.52" x2="99.06" y2="96.52" width="0.1524" layer="91"/>
<wire x1="99.06" y1="96.52" x2="99.06" y2="86.36" width="0.1524" layer="91"/>
<wire x1="99.06" y1="86.36" x2="111.76" y2="86.36" width="0.1524" layer="91"/>
<junction x="111.76" y="86.36"/>
<label x="116.84" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="8"/>
<wire x1="-68.58" y1="91.44" x2="-71.12" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="91.44" x2="-71.12" y2="73.66" width="0.1524" layer="91"/>
<pinref part="X5" gate="-20" pin="S"/>
<wire x1="-91.44" y1="73.66" x2="-96.52" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="73.66" x2="-91.44" y2="73.66" width="0.1524" layer="91"/>
<junction x="-91.44" y="73.66"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="J5" gate="G$2" pin="7"/>
<wire x1="-68.58" y1="33.02" x2="-86.36" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="33.02" x2="-86.36" y2="38.1" width="0.1524" layer="91"/>
<pinref part="X5" gate="-6" pin="S"/>
<wire x1="-91.44" y1="38.1" x2="-96.52" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="38.1" x2="-91.44" y2="38.1" width="0.1524" layer="91"/>
<junction x="-91.44" y="38.1"/>
</segment>
</net>
<net name="PWM_LANCEUR_MAN" class="0">
<segment>
<pinref part="X4" gate="-2" pin="S"/>
<wire x1="-68.58" y1="-12.7" x2="-66.04" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="-12.7" x2="-66.04" y2="-5.08" width="0.1524" layer="91"/>
<label x="-66.04" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X15" gate="-2" pin="S"/>
<wire x1="48.26" y1="170.18" x2="53.34" y2="170.18" width="0.1524" layer="91"/>
<wire x1="53.34" y1="170.18" x2="53.34" y2="175.26" width="0.1524" layer="91"/>
<label x="53.34" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOCTL" class="0">
<segment>
<pinref part="X3" gate="-8" pin="S"/>
<wire x1="-55.88" y1="154.94" x2="-35.56" y2="154.94" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="154.94" x2="-35.56" y2="147.32" width="0.1524" layer="91"/>
<label x="-35.56" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-26" pin="S"/>
<wire x1="111.76" y1="45.72" x2="116.84" y2="45.72" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$2" pin="5"/>
<wire x1="88.9" y1="35.56" x2="99.06" y2="35.56" width="0.1524" layer="91"/>
<wire x1="99.06" y1="35.56" x2="99.06" y2="45.72" width="0.1524" layer="91"/>
<wire x1="99.06" y1="45.72" x2="111.76" y2="45.72" width="0.1524" layer="91"/>
<junction x="111.76" y="45.72"/>
<label x="116.84" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDSSDFD" class="0">
<segment>
<pinref part="X5" gate="-14" pin="S"/>
<wire x1="-91.44" y1="58.42" x2="-96.52" y2="58.42" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$2" pin="3"/>
<wire x1="-68.58" y1="43.18" x2="-76.2" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="43.18" x2="-76.2" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="58.42" x2="-91.44" y2="58.42" width="0.1524" layer="91"/>
<junction x="-91.44" y="58.42"/>
</segment>
</net>
<net name="SDFSDFSGD" class="0">
<segment>
<pinref part="X5" gate="-10" pin="S"/>
<wire x1="-91.44" y1="48.26" x2="-96.52" y2="48.26" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$2" pin="5"/>
<wire x1="-68.58" y1="38.1" x2="-81.28" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="38.1" x2="-81.28" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="48.26" x2="-91.44" y2="48.26" width="0.1524" layer="91"/>
<junction x="-91.44" y="48.26"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
